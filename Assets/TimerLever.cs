﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerLever : MonoBehaviour {

	public GameObject Player;
	public Animator animator;
    public static bool playerInRange = false;
    public AudioSource audios;
    public AudioClip leverSound;
    public float volume;

	void Start ()
	{
		animator = GetComponent<Animator> ();
	}

	private void OnTriggerEnter(Collider other)
	{
		OnTriggerStay (other);
        
    }

	private void OnTriggerStay(Collider other)
	{
		if(other.gameObject.Equals(Player))
		{
            playerInRange = true;
            if (Input.GetKeyDown(KeyCode.F))
			{
				animator.SetBool ("IsPushed", true);
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		animator.SetBool ("IsPushed", false);
        playerInRange = false;
    }

    void LeverPlaySound()
    {
        audios.PlayOneShot(leverSound, volume);
    }
}
