﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class killer_water : MonoBehaviour {

    public float movementSpeed = 10;
    public float endHeight;
    public GameObject player;
    public GameObject skull;

    public GameObject secondaryObject = null;


    public float startHeight;

    public int direction = 0;

	// Use this for initialization
	void Start () {
		
	}

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.gameObject.tag.Equals("PlayerSkeletonFull"))
    //    {
    //        player = other.gameObject;
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.tag.Equals("PlayerSkeletonFull"))
    //    {
    //        player = null;
    //    }
    //}

    //private void OnTriggerStay(Collider other)
    //{
    //    if (other.gameObject.tag.Equals("PlayerSkeletonFull"))
    //    {
    //        player = other.gameObject;
    //    }
    //}

    public GameObject GameObject;
    bool check = false;

    // Update is called once per frame
    void Update ()
    {
        if (direction == 1)
        {
            if (transform.position.y < endHeight)
            {
                transform.Translate(Vector3.up * movementSpeed * Time.deltaTime);

                if (secondaryObject != null)
                    secondaryObject.transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
            }
        }

        if (direction == -1)
        {
            if (transform.position.y > startHeight)
            {
                transform.Translate(Vector3.down * movementSpeed * Time.deltaTime);
                if (secondaryObject != null)
                    secondaryObject.transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
            }
        }

        //if (player)
        //{
        //    // else 
        //    if (player.activeSelf == true)
        //    {
        //        if (player.transform.position.y + 1.3 < transform.position.y)
        //        {
        //            //player.SetActive(false);         

        //            //Invoke("SendPlayerToCheckPoint", 0.5f);
        //            //ObjectToTeleport.transform.position = new Vector3(878.84f, 38.38f, -85.9f);
        //            //player.transform.position = GameObject.transform.position;
        //            StartCoroutine(StopPlayer());
        //        }
        //    }

        //    if (skull.activeSelf == true)
        //    {
        //        if (skull.transform.position.y + 1.3 < transform.position.y)
        //        {
        //            //player.SetActive(false);


        //            //Invoke("SendPlayerToCheckPoint", 0.5f);
        //            //skull.gameObject.transform.position = GameObject.transform.position;
        //            StartCoroutine(StopSkull());
        //            // ObjectToTeleport.transform.position = new Vector3(878.84f, 38.38f, -85.9f);

        //        }
        //    }
        //}
    }

    //private void SendPlayerToCheckPoint()
    //{
    //   // player.transform.position = CheckPoint.GetActiveCheckPointPosition();
    //}

    public void WaterUp()
    {
        direction = 1;
    }

    public void WaterDown()
    {
        direction = -1;
    }


    //IEnumerator StopPlayer()
    //{
    //    if (check == false)
    //    {
    //        check = true;
    //        //if (partitionsActivated == false) EnablePartitionSystem();
    //        FindObjectOfType<GameManager>().EndGame();
    //        yield return new WaitForSeconds(2);
    //        player.transform.position = GameObject.transform.position;
    //        check = false;
    //        //itemThrownToIgnite = false;
    //        //Destroy(gameObject);

    //    }
    //}

    //IEnumerator StopSkull()
    //{
    //    //if (partitionsActivated == false) EnablePartitionSystem();
    //    FindObjectOfType<GameManager>().EndGame();
    //    yield return new WaitForSeconds(2);
    //    player.transform.position = GameObject.transform.position;
    //    //itemThrownToIgnite = false;
    //    //Destroy(gameObject);
    //}
}
