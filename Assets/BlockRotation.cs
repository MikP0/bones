﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockRotation : MonoBehaviour {

	public Rigidbody rb;
	public GameObject SecondBox;

	void Start()
	{
		rb = GetComponent<Rigidbody> ();
	}

	void OnTriggerEnter(Collider other)
	{
		rb.constraints = RigidbodyConstraints.FreezeRotationZ;
	}
		
}
