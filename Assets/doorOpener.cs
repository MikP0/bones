﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorOpener : MonoBehaviour {

    public GameObject puller1;
    public GameObject puller2;
    public GameObject puller3;
    public GameObject door;
    public Animator animator;
    private GameObject effectInstance;
    bool doorOpened = false;
    public GameObject effect;

    // Use this for initialization
    void Start () {
        //puller1 = GetComponent<GameObject>();
        //puller2 = GetComponent<GameObject>();
        //puller3 = GetComponent<GameObject>();
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
        if(puller1.GetComponent<doorPuller>().state == true &&
            puller2.GetComponent<doorPuller>().state == true &&
            puller3.GetComponent<doorPuller>().state == true)
            door.SetActive(false);

	}
}
