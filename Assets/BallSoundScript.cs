﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSoundScript : MonoBehaviour {
	public AudioClip first;
	public AudioClip second;
	public AudioSource audioSource;
	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
		PlaySound ();
		Invoke ("PlaySound", 3);
		Invoke ("PlaySound2", 6.3f);
		//audioSource.time = 0.0f;
		//audioSource.Play ();
		//audioSource.SetScheduledEndTime (AudioSettings.dspTime + (15.0f));
		//audioSource.SetScheduledEndTime (AudioSettings.dspTime + (15.0f));
	}
	
	// Update is called once per frame
	void PlaySound(){
		audioSource.PlayOneShot (first);

	}

	void PlaySound2(){
		audioSource.PlayOneShot (second);

	}
}
