﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {
    public GameObject spawner;
    void OnTriggerStay(Collider player)
    {
        if (player.tag == "PlayerSkeletonFull")
        {
            player.transform.position = new Vector3(spawner.transform.position.x, spawner.transform.position.y, spawner.transform.position.z);

        }
    }
}
