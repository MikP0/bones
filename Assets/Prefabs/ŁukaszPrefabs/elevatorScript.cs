﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class elevatorScript : MonoBehaviour {
	public GameObject elevator;
	public GameObject activator;
	public GameObject activator2;

	private float x,x1,y,y1,z,z1;
	void Start()
	{
		x = activator.transform.position.x;
		y = activator.transform.position.y;
		z = activator.transform.position.z;
		x1 = activator2.transform.position.x;
		y1 = activator2.transform.position.y;
		z1 = activator2.transform.position.z;
	}

	void OnTriggerStay(Collider player)
    {
		if (player.tag == "PlayerSkeletonFull") {
				activator.transform.position = new Vector3 (x, y - 0.05f, z);
				activator2.transform.position = new Vector3 (x, y - 0.05f, z);
				elevator.transform.position += elevator.transform.up * Time.deltaTime;

			}
        else if (player.tag == "RightArm")
        {
            activator.transform.position = new Vector3(x, y - 0.05f, z);
            activator2.transform.position = new Vector3(x, y - 0.05f, z);
            elevator.transform.position += elevator.transform.up * Time.deltaTime;

        }
        else if (player.tag == "PlayerSkull")
        {
            activator.transform.position = new Vector3(x, y - 0.05f, z);
            activator2.transform.position = new Vector3(x, y - 0.05f, z);
            elevator.transform.position += elevator.transform.up * Time.deltaTime;

        }
    }

	void OnTriggerExit()
	{
		activator.transform.position = new Vector3 (x, y, z);
		activator2.transform.position = new Vector3 (x, y, z);
	}

}
