﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeVolume : MonoBehaviour {

    public Slider sliderMusic;
    public GameObject musicSourceDonor;

    void Start()
    {
        sliderMusic.value = musicSourceDonor.GetComponent<AudioSource>().volume;
    }
    void Update()
    {
        musicSourceDonor.GetComponent<AudioSource>().volume= sliderMusic.value;
    }
}
