﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour {

    public GameObject menu;
    public GameObject VolumeSlider;
    public GameObject VolumeText;
    public GameObject lvl1;
    public GameObject lvl2;
    public GameObject lvl3;
    public GameObject checkpoint = null;
    bool status = false;

    void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Cursor.visible = !Cursor.visible;

            if (!menu.activeInHierarchy) Time.timeScale = 0.0f;
            if (menu.activeInHierarchy) Time.timeScale = 1.0f;
            menu.SetActive(!menu.activeInHierarchy);
        }
    }

    public void continueGame()
    {
        menu.SetActive(false);
        VolumeSlider.SetActive(false);
        VolumeText.SetActive(false);
        lvl1.SetActive(false);
        lvl2.SetActive(false);
        lvl3.SetActive(false);
        Time.timeScale = 1.0f;
        Cursor.visible = !Cursor.visible;
    }

    public void quitGame()
    {
        Application.Quit();
    }

    public void optionsMenu()
    {
            VolumeSlider.SetActive(!VolumeSlider.activeInHierarchy);
            VolumeText.SetActive(!VolumeText.activeInHierarchy);
 
    }

    public void playLevel()
    {
        lvl1.SetActive(!lvl1.activeInHierarchy);
        lvl2.SetActive(!lvl2.activeInHierarchy);
        lvl3.SetActive(!lvl3.activeInHierarchy);
    }

    public void playLevel1()
    {
        SceneManager.LoadScene(1);
        Time.timeScale = 1.0f;
    }
    public void playLevel2()
    {
        SceneManager.LoadScene(2);
        Time.timeScale = 1.0f;
    }
    public void playLevel3()
    {
        SceneManager.LoadScene(3);
        Time.timeScale = 1.0f;
    }
    public void backToCP()
    {
        //GameObject.FindGameObjectWithTag("PlayerSkeletonFull").transform.position = checkpoint.transform.position;
        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
        menu.SetActive(false);
        Time.timeScale = 1.0f;
        Cursor.visible = !Cursor.visible;
    }
}
