﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetpackActivator : MonoBehaviour {

	public GameObject jetpackScript;
	public GameObject jetpack;
	public GameObject ActualJetpack = null;
    //public GameObject gate;
    public bool ActivateOrDeactivate;

    public int durationTime = 10;

	void OnTriggerEnter(Collider player) 
	{
		if (player.tag == "PlayerSkeletonFull")
        {
			jetpackScript.SetActive (ActivateOrDeactivate);
			jetpack.SetActive (ActivateOrDeactivate);
			//ActualJetpack.SetActive (ActivateOrDeactivate);
            jetpackScript.GetComponent<Jetpack>().timerCanvas.SetActive(true);
           // ActualJetpack.GetComponent<Jetpack>().timerCanvas.SetActive(true);
            //if(gate.transform.position.y>=-5.3f)

            jetpackScript.GetComponent<Jetpack>().fireUp(durationTime);

            //if (gate != null)
            //{
            //    gate.transform.position = new Vector3(gate.transform.position.x, -5.3f, gate.transform.position.z);
            //}
        }
        
	}

    public void active()
    {
        jetpackScript.SetActive(ActivateOrDeactivate);
        jetpack.SetActive(ActivateOrDeactivate);
        jetpackScript.GetComponent<Jetpack>().timerCanvas.SetActive(true);
        jetpackScript.GetComponent<Jetpack>().fireUp(durationTime);
    }

    public void deActive()
    {
        jetpackScript.GetComponent<Jetpack>().timerCanvas.SetActive(false);
        jetpackScript.SetActive(false);
        jetpack.SetActive(false);
    }
}
