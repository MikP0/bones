﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JetpackDeath : MonoBehaviour
{
    public GameObject teleportPoint;
    public GameObject jetpackScript;
    public GameObject ActualJetpack;
    void OnTriggerEnter(Collider player)
    {

        if (player.tag == "PlayerSkeletonFull")
        {
            player.transform.position = teleportPoint.transform.position;
            jetpackScript.GetComponent<Jetpack>().ifDied();
            ActualJetpack.GetComponent<Jetpack>().ifDied();
        }
    }
}
