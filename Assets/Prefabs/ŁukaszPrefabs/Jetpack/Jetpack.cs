﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jetpack : MonoBehaviour {

	public GameObject fuel;
    public GameObject fuelRigged;
    public GameObject donor;
    public GameObject timerCanvas;
    public GameObject text;
    public int JetpackDuration = 10;
	private GameObject bone;
    public float timePassed;
    public float cooldown = 0.05f;

    public GameObject FlyRotBones = null;

    private List<GameObject> lista = new List<GameObject>();
    private List<GameObject> listaRigs = new List<GameObject>();


    public bool isCoroutineStarted = false;

    float timer = 0.0f;
    int timerInt = 0;
    private float fuelCooldown = 0.0f;

    public void ifDied()
    {
       // timePassed = 0;
       // isCoroutineStarted = false;
    }


    bool fired = false;

    public void fireUp(int duration)
    {
        fired = true;
        timerCanvas.SetActive(true);
        timer = 0.0f;
        JetpackDuration = duration;


        if (FlyRotBones != null)
        {
            FlyRotBones.SetActive(true);
        }
    }

    public void Update()
    {
        if ((fired) && (timer <= JetpackDuration))
        {
            timer += Time.deltaTime;

            timerInt = Convert.ToInt32(timer);
            //text.GetComponent<Text>().text = "Time left : " + (JetpackDuration - timerInt).ToString() + "s";
            text.GetComponent<Text>().text = (JetpackDuration - timerInt).ToString() + "s";
            if (Time.time > fuelCooldown)
            {

                if ((Input.GetMouseButton(0)) || (Input.GetKey(KeyCode.LeftControl)))
                {
                    if (lista.Count < 20)
                        lista.Add((GameObject)Instantiate(fuel, donor.transform.position, Quaternion.identity));
                 
                    if (listaRigs.Count < 8)
                        listaRigs.Add((GameObject)Instantiate(fuelRigged, donor.transform.position, Quaternion.identity));
                }


                for (int i = 0; i < lista.Count; i++)
                {
                    if (
                        Mathf.Abs(lista[i].transform.position.x - donor.transform.position.x) > 2.0f
                        || Mathf.Abs(lista[i].transform.position.y - donor.transform.position.y) > 2.0f
                        || Mathf.Abs(lista[i].transform.position.z - donor.transform.position.z) > 1.0f)
                    {
                        Destroy(lista[i]);
                        lista.Remove(lista[i]);
                    }
                }


                for (int i = 0; i < listaRigs.Count; i++)
                {
                    if (
                        Mathf.Abs(listaRigs[i].transform.position.x - donor.transform.position.x) > 2.0f
                        || Mathf.Abs(listaRigs[i].transform.position.y - donor.transform.position.y) > 2.0f
                        || Mathf.Abs(listaRigs[i].transform.position.z - donor.transform.position.z) > 1.0f)
                    {
                        Destroy(listaRigs[i]);
                        listaRigs.Remove(listaRigs[i]);
                    }
                }
                fuelCooldown = Time.time + cooldown;
            }

        }
        else if (fired)
        {
            fired = false;
            timerCanvas.SetActive(false);

            if (FlyRotBones != null)
            {
                FlyRotBones.SetActive(false);
            }
        }
    }


    //public IEnumerator GoLeft()
    //{
    //    timePassed = 0;
    //    isCoroutineStarted = true;

    //    while (timePassed < JetpackDuration)
    //    {
    //        if (Input.GetMouseButton(0))
    //        {
    //            if (lista.Count < 30)
    //                lista.Add((GameObject)Instantiate(fuel, donor.transform.position, Quaternion.identity));

    //        }
    //        for (int i = 0; i < lista.Count; i++)
    //        {
    //            if (
    //                Mathf.Abs(lista[i].transform.position.x - donor.transform.position.x) > 3.0f
    //                || Mathf.Abs(lista[i].transform.position.y - donor.transform.position.y) > 3.0f
    //                || Mathf.Abs(lista[i].transform.position.z - donor.transform.position.z) > 3.0f)
    //            {
    //                Destroy(lista[i]);
    //                lista.Remove(lista[i]);
    //            }
    //        }
    //        timePassed += Time.deltaTime;
    //        yield return null;
    //    }
    //}



    //public void FixedUpdate()
    //{
    //    if (!isCoroutineStarted)
    //    {
    //        timerCanvas.SetActive(true);
    //        StartCoroutine(GoLeft());
    //    }

    //    timer = Convert.ToInt32(timePassed);
    //    text.GetComponent<Text>().text = "Time left : " + (JetpackDuration - timer).ToString() + "s";


    //    for (int i = 0; i < lista.Count; i++)
    //    {
    //        if (
    //            Mathf.Abs(lista[i].transform.position.x - donor.transform.position.x) > 3.0f
    //            || Mathf.Abs(lista[i].transform.position.y - donor.transform.position.y) > 3.0f
    //            || Mathf.Abs(lista[i].transform.position.z - donor.transform.position.z) > 3.0f)
    //        {
    //            Destroy(lista[i]);
    //            lista.Remove(lista[i]);
    //        }
    //    }
    //}


    //public IEnumerator GoLeft()
    //{
    //    timePassed = 0;
    //    isCoroutineStarted = true;

    //    while (timePassed < JetpackDuration)
    //    {
    //        if (Input.GetMouseButton(0))
    //        {
    //            if (lista.Count < 30)
    //                lista.Add((GameObject)Instantiate(fuel, donor.transform.position, Quaternion.identity));

    //        }
    //        for (int i = 0; i < lista.Count; i++)
    //        {
    //            if (
    //                Mathf.Abs(lista[i].transform.position.x - donor.transform.position.x) > 3.0f
    //                || Mathf.Abs(lista[i].transform.position.y - donor.transform.position.y) > 3.0f
    //                || Mathf.Abs(lista[i].transform.position.z - donor.transform.position.z) > 3.0f)
    //            {
    //                Destroy(lista[i]);
    //                lista.Remove(lista[i]);
    //            }
    //        }
    //        timePassed += Time.deltaTime;
    //        yield return null;
    //    }
    //}
}
