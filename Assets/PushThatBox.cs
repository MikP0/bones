﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushThatBox : MonoBehaviour
{
    public float pushPower = 2.0f;


    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = GetComponent<Rigidbody>();

        if ((body == null) || (body.isKinematic))
        {
            return;
        }
        

        if (hit.moveDirection.y < -0.3f)
        {
            return;
        }

        Vector3 dir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        body.velocity = dir * pushPower;
    }
}
