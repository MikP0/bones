﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableBox2 : MonoBehaviour {

	public GameObject Player;
	public GameObject otherSide;
	public GameObject rightBorder;
	private float timeSpent;

	void Start()
	{
		//otherSide = GetComponent<GameObject> ();
		//Player = GetComponent<GameObject> ();
	}

	private void OnTriggerEnter(Collider other)
	{
		OnTriggerStay (other);
	}

	private void OnTriggerStay(Collider other)
	{
		timeSpent = Time.deltaTime;
		if(Input.GetKeyDown(KeyCode.F))
		{
			transform.Translate (Vector3.left * 5.0f * timeSpent);
			otherSide.transform.Translate (Vector3.right * 5.0f * timeSpent);
		}
		else 
		{	
			if (other.gameObject == Player) {

				transform.Translate (Vector3.right * 5.0f * timeSpent);
				otherSide.transform.Translate (Vector3.left * 5.0f * timeSpent);
			}
			if (other.gameObject == rightBorder) 
			{
				transform.Translate (Vector3.right * 5.0f * timeSpent);
				otherSide.transform.Translate (Vector3.left * 5.0f * timeSpent);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.gameObject == Player) 
		{

		}

	}
}