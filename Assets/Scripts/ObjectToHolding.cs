﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectToHolding : MonoBehaviour {

    public Vector3 localPositionOfItemHeld = Vector3.zero;
    public Vector3 localEulerAngles = new Vector3(25, 0, 0);
    public GameObject holdingSpotObject;
    public float range = 2;
    public float dist;

    public static GameObject objectHeld;
    public static bool itemInRange = false;
    public static bool lowerUsagePriority = false;

    private GameObject player;
    private Transform parentTransform;
    private float buttonTimeOffset = 0;
    private bool action = true;
    private bool actionDone = false;
    private static List<ObjectToHolding> itemsToLift = new List<ObjectToHolding>();
    // Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("PlayerSkeletonFull");
        //holdingSpotObject = player.transform.Find("Bip01 L Finger0").gameObject;
        parentTransform = transform.parent;
        itemsToLift.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        CalculateDistanceToPlayer();

        CheckIfItemInRange(dist);
        ThrowItem();
        PickUpItem();

        CheckIfTheActionCanBePerformedAgain();
    }
    void CalculateDistanceToPlayer()
    {
        if (player.active == true && objectHeld == null)
        {
            dist = Vector3.Distance(player.transform.position, gameObject.transform.position);
        }
    }
    void PickUpItem()
    {
        if (dist < range)
        {
            if (Input.GetKey("f") && objectHeld == null && action == true && (player.active == true))
            {
                transform.parent = holdingSpotObject.transform;
                objectHeld = gameObject;
                objectHeld.transform.localPosition = localPositionOfItemHeld;
                objectHeld.transform.localEulerAngles = localEulerAngles;
                SetRigidbody(false);
                buttonTimeOffset = Time.time + 0.5f;
                actionDone = true;
            }
        }
    }
    //this method is used to display usage message in UI
    void CheckIfItemInRange(float dist) 
    {
        if (GetDistanceToTheNearestItem() < range)
        {
            //Debug.Log(dist);
            itemInRange = true;
        } 
        else
            itemInRange = false;
    }

    float GetDistanceToTheNearestItem()
    {
        return itemsToLift.Min(item => item.dist);
    }
    void CheckIfTheActionCanBePerformedAgain()
    {
        if (Time.time > buttonTimeOffset && actionDone == true)
        {
            action = !action;
            actionDone = false;
        }
    }
    
    
    void ThrowItem()
    {
        if (Input.GetKey("f") && objectHeld == gameObject && action == false && lowerUsagePriority == false)
        {
            ThrowItemHeld();
            buttonTimeOffset = Time.time + 0.5f;
        }
    }

    public void ThrowItemHeld()
    {
        // dodałem to, bo jak szkielet nie dotknął jeszcze pochodni
        // a odczepiał głowe to się to wywyoływało i się psuło
        if (transform.parent != parentTransform)
        {
            transform.parent = parentTransform;
            objectHeld = null;
            SetRigidbody(true);
            actionDone = true;
        }
        else
        {
            objectHeld = null;
            SetRigidbody(true);
            actionDone = false;
        }
    }
    void SetRigidbody(bool state)
    {
        Rigidbody rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = state;
        rigidbody.isKinematic = !state;

    }
    private void OnDestroy()
    {
        itemInRange = false;
        itemsToLift.Remove(this);
    }
}
