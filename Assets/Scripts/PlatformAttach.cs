﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformAttach : MonoBehaviour
{
	public GameObject PlayerScene;

	private void OnTriggerEnter(Collider other)
	{
        if (other.gameObject.name != "ColliderToRigidbody")
        {
            if (other.gameObject.tag == "PlayerSkeletonFull")
            {
                other.transform.parent = transform;
            }
            else if (other.gameObject.tag == "PlayerSkull")
            {
                other.transform.parent = transform;
            }
            else if (other.gameObject.tag == "RightArm")
            {
                other.transform.parent = transform;
            }
            else if (other.gameObject.tag == "PartBody")
            {
                other.transform.parent = transform;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name != "ColliderToRigidbody")
        {
            if (other.gameObject.tag == "PlayerSkeletonFull")
            {
                other.transform.parent = transform;
            }
            else if (other.gameObject.tag == "PlayerSkull")
            {
                other.transform.parent = transform;
            }
            else if (other.gameObject.tag == "RightArm")
            {
                other.transform.parent = transform;
            }
            else if (other.gameObject.tag == "PartBody")
            {
                other.transform.parent = transform;
            }
        }
    }

    private void OnTriggerExit(Collider other)
	{
        if (other.gameObject.name != "ColliderToRigidbody")
        {
            if (other.gameObject.tag == "PlayerSkeletonFull")
            {
                other.transform.parent = PlayerScene.transform;
            }
            else if (other.gameObject.tag == "PlayerSkull")
            {
                other.transform.parent = PlayerScene.transform;
            }
            else if (other.gameObject.tag == "RightArm")
            {
                other.transform.parent = PlayerScene.transform;
            }
            else if (other.gameObject.tag == "PartBody")
            {
                other.transform.parent = PlayerScene.transform;
            }
        }
    }
}
