﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateTrigger : MonoBehaviour
{
    public GameObject gate = null;
    public GameObject withc = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            //transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
            gate.SetActive(true);


            if (withc != null)
            {
                withc.GetComponent<MageShooter>().soundActive = false;
            }
        }
    }
}
