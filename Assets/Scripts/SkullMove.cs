﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullMove : MonoBehaviour
{
    public float speed = 3.0f;
    public float maxSpeed = 7.0f;
    public int resetLevel = -10;

    float rotationSpeed = 5.0f;
    float xSpeed;
    float additonalSpeed;
    float velocity;
    float distToGround;
    public static bool pickUp = false;
    public static bool merged = false;

    Rigidbody mRigidbody;

    GameObject[] gameObjects;
    GameObject closest;
    float distance;
    float curDistance;
    Vector3 position;
    Vector3 diff;
    // Use this for initialization
    void Start ()
    {
        mRigidbody = GetComponent<Rigidbody>();
        xSpeed = 0.0f;
        additonalSpeed = 0.0f;
        velocity = 0.0f;
        distToGround = GetComponent<Collider>().bounds.extents.y;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (transform.position.y <= resetLevel)                     // DEAD
        {
            Debug.Log("SPADAM");
            //FindObjectOfType<GameManager>().EndGame();
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
        }

        if (isActiveAndEnabled)
        {
            if ((Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.A)))
            {
                mRigidbody.constraints = RigidbodyConstraints.None;
                mRigidbody.constraints = RigidbodyConstraints.FreezePositionZ;             
                xSpeed = 1;
                additonalSpeed += speed;
                mRigidbody.AddForce(new Vector3(velocity, 0, 0));
                mRigidbody.AddTorque(new Vector3(0, 0, xSpeed * rotationSpeed));
            }
            if ((Input.GetKey(KeyCode.RightArrow)) || (Input.GetKey(KeyCode.D)))
            {
                mRigidbody.constraints = RigidbodyConstraints.None;
                mRigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
                xSpeed = -1;
                additonalSpeed += speed;
                mRigidbody.AddForce(new Vector3(velocity, 0, 0));
                mRigidbody.AddTorque(new Vector3(0, 0, xSpeed * rotationSpeed));
            }

            if ((Input.GetKeyUp(KeyCode.RightArrow)) || (Input.GetKeyUp(KeyCode.D)) || (Input.GetKeyUp(KeyCode.LeftArrow)) || (Input.GetKeyUp(KeyCode.A)))
            {
                mRigidbody.constraints = RigidbodyConstraints.None;
                mRigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
                xSpeed = 0;
                additonalSpeed = 0;              
            }

            if ((Input.GetKeyDown(KeyCode.DownArrow)) || (Input.GetKeyDown(KeyCode.S)))
            {
                if (isGrounded())
                {
                    mRigidbody.constraints = RigidbodyConstraints.FreezeRotationZ;
                    mRigidbody.constraints = RigidbodyConstraints.FreezeRotationX;
                    mRigidbody.constraints = RigidbodyConstraints.FreezeRotationY;
                    mRigidbody.constraints = RigidbodyConstraints.FreezePositionX;
                    mRigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
                    mRigidbody.AddForce(new Vector3(0, 0, 0));
                    mRigidbody.AddTorque(new Vector3(0, 0, 0));
                    mRigidbody.velocity = Vector3.zero;
                }

                xSpeed = 0;
                additonalSpeed = 0;                           
            }

            // movement           
            velocity = xSpeed * (-1) * Time.deltaTime * additonalSpeed;
            if (velocity > maxSpeed)
                velocity = maxSpeed;
            else if (velocity < -maxSpeed)
                velocity = -maxSpeed;

            //mRigidbody.AddForce(new Vector3(velocity, 0, 0));
            //mRigidbody.AddTorque(new Vector3(0, 0, xSpeed  * rotationSpeed));

            gameObjects = GameObject.FindGameObjectsWithTag("PartBody");
            closest = null;
            distance = Mathf.Infinity;
            position = transform.position;

            foreach (GameObject go in gameObjects)
            {
                diff = go.transform.position - position;
                curDistance = diff.sqrMagnitude;
                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }
            //Debug.Log(calculateDistanceToTheNearest(closest) + " " + pickUp.ToString() + " " + merged.ToString());
            merged = false;
            if (calculateDistanceToTheNearest(closest) < 1.2f)
            {
                pickUp = true;
            }
            if (calculateDistanceToTheNearest(closest) >= 1.2f)
            {
                pickUp = false;
            }

            //if ((Input.GetKeyDown(KeyCode.E) && checkIfMergePossible(closest)))
            if ((Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.E)) && checkIfMergePossible(closest))
            {
                merged = true;
                //FindObjectOfType<GameMaster>().BodyToSkull(closest);
                GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().BodyToSkull(closest);
            }

            if (mRigidbody.position.y <= resetLevel)                     // DEAD
            {
                //FindObjectOfType<GameManager>().EndGame();
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            }
        }     
    }

    bool checkIfMergePossible(GameObject closest)
    {
        return calculateDistanceToTheNearest(closest) < 1.2f;
    }

    float calculateDistanceToTheNearest(GameObject closest)
    {
        return Vector3.Distance(transform.position, closest.transform.position);
    }
    bool isGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    private void OnEnable()
    {
        mRigidbody = GetComponent<Rigidbody>();
        mRigidbody.constraints = RigidbodyConstraints.FreezeRotationZ;
        xSpeed = 0;
        mRigidbody.velocity = Vector3.zero;
    }
}