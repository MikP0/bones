﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    public GameObject SkeletonPlayer;
    public GameObject SkullPlayer;
    public GameObject RightArm;
    public GameObject BodyPart;
    public GameObject BodyPartArm;

    public GameObject jetpackSor = null;
    public GameObject jetpackAct = null;


    GameObject controledArm = null;

    float tempX;
    float tempY;
    float tempZ;

    public int globalSkelonMode = 0;

    // public bool jetPackMode = false;


    public GameObject getLastBody()
    {
        return remLastBody;
    }

    GameObject remLastBody = null;


    public void BodyToSkull(GameObject gameObject)
    {
        Debug.Log("LINK");
        tempX = SkullPlayer.transform.position.x;
        tempY = SkullPlayer.transform.position.y - 0.3f;
        tempZ = SkullPlayer.transform.position.z;

        BodyPartObjectScript bodyPartObjectScript = gameObject.GetComponent<BodyPartObjectScript>();

        if ((globalSkelonMode == 0) && (bodyPartObjectScript.arm))
        {
            SkeletonPlayer.SetActive(true);
            SkeletonPlayer.transform.SetPositionAndRotation(new Vector3(tempX, tempY, tempZ), SkeletonPlayer.transform.rotation);
            //FindObjectOfType<CameraScrpit>().changeFollower(SkeletonPlayer);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScrpit>().changeFollower(SkeletonPlayer);
        }        
        else if ((globalSkelonMode == 0) && (!bodyPartObjectScript.arm))
        {
            SkeletonPlayer.SetActive(true);
            //FindObjectOfType<SkeletonMove>().swapBody();
            SkeletonPlayer.gameObject.GetComponent<SkeletonMove>().swapBody();
            //GameObject.FindGameObjectWithTag("PlayerSkeletonFull").GetComponent<SkeletonMove>().swapBody();
            SkeletonPlayer.transform.SetPositionAndRotation(new Vector3(tempX, tempY, tempZ), SkeletonPlayer.transform.rotation);
            //FindObjectOfType<CameraScrpit>().changeFollower(SkeletonPlayer);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScrpit>().changeFollower(SkeletonPlayer);
        }
        else if ((globalSkelonMode == 30) && (bodyPartObjectScript.arm))
        {
            SkeletonPlayer.SetActive(true);
            //FindObjectOfType<SkeletonMove>().swapBody();
            SkeletonPlayer.gameObject.GetComponent<SkeletonMove>().swapBody();
            //GameObject.FindGameObjectWithTag("PlayerSkeletonFull").GetComponent<SkeletonMove>().swapBody();
            SkeletonPlayer.transform.SetPositionAndRotation(new Vector3(tempX, tempY, tempZ), SkeletonPlayer.transform.rotation);
            //FindObjectOfType<CameraScrpit>().changeFollower(SkeletonPlayer);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScrpit>().changeFollower(SkeletonPlayer);
        }
        else if ((globalSkelonMode == 30) && (!bodyPartObjectScript.arm))
        {
            SkeletonPlayer.SetActive(true);
            SkeletonPlayer.transform.SetPositionAndRotation(new Vector3(tempX, tempY, tempZ), SkeletonPlayer.transform.rotation);
            //FindObjectOfType<CameraScrpit>().changeFollower(SkeletonPlayer);
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScrpit>().changeFollower(SkeletonPlayer);
        }
        else
        {
            Debug.Log(globalSkelonMode + "\t\t\t" + bodyPartObjectScript.arm);
        }

        remLastBody = null;
        Destroy(gameObject, 0.01f);
        SkullPlayer.SetActive(false);
    }

    public void SkullAndBody(int mode)
    {
        Debug.Log("SEPARATE");


        tempX = SkeletonPlayer.transform.position.x;
        tempY = SkeletonPlayer.transform.position.y;
        tempZ = SkeletonPlayer.transform.position.z;

       
        ObjectToHolding tmp = FindObjectOfType<ObjectToHolding>();
        if (tmp)
            tmp.ThrowItemHeld();
        SkullPlayer.SetActive(true);
        SkullPlayer.transform.SetPositionAndRotation(new Vector3(tempX, tempY + 1.5f, tempZ), SkullPlayer.transform.rotation);

        if (globalSkelonMode == 0)
        {
            BodyPart.SetActive(true);
            remLastBody = Instantiate(BodyPart, new Vector3(tempX, tempY, tempZ), BodyPart.transform.rotation);
            BodyPart.SetActive(false);

        }
        else if (globalSkelonMode == 30)
        {
            BodyPartArm.SetActive(true);
            remLastBody = Instantiate(BodyPartArm, new Vector3(tempX, tempY, tempZ), BodyPartArm.transform.rotation);
            BodyPartArm.SetActive(false);
        }

        //FindObjectOfType<CameraScrpit>().changeFollower(SkullPlayer);
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScrpit>().changeFollower(SkullPlayer);

        //if (GameObject.FindGameObjectWithTag("JetpackSource"))
        //    GameObject.FindGameObjectWithTag("JetpackSource").GetComponent<Jetpack>().FlyRotBones.SetActive(false);

        //if (GameObject.FindGameObjectWithTag("JetpackActivator"))
        //    GameObject.FindGameObjectWithTag("JetpackActivator").GetComponent<JetpackActivator>().deActive();


        if (jetpackSor != null)
            jetpackSor.GetComponent<Jetpack>().FlyRotBones.SetActive(false);

        if (jetpackAct != null)
            jetpackAct.GetComponent<JetpackActivator>().deActive();


        SkeletonPlayer.SetActive(false);
    }

    public GameObject DropRightArm(int mode)
    {
        if (mode == 0)
        {
            Debug.Log("Drop Right Arm");
            tempX = SkeletonPlayer.transform.position.x;
            tempY = SkeletonPlayer.transform.position.y;
            tempZ = SkeletonPlayer.transform.position.z;

            //GameObject.FindGameObjectWithTag("PlayerSkeletonFull").GetComponent<SkeletonMove>().swapBody();
            //FindObjectOfType<SkeletonMove>().swapBody();
            SkeletonPlayer.gameObject.GetComponent<SkeletonMove>().swapBody();


            globalSkelonMode = 30;

            RightArm.SetActive(true);
            GameObject newRightArm = Instantiate(RightArm, new Vector3(tempX + 0.3f, tempY + 1.0f, tempZ + 0.3f), RightArm.transform.rotation);
            RightArm.SetActive(false);

            controledArm = newRightArm;

            return newRightArm;
        }

        return null;
    }


    public void GetArm(int mode, GameObject arm)
    {
        if (mode == 30)
        {
            Debug.Log("Collect Right Arm");

            globalSkelonMode = 0;
            SkeletonPlayer.gameObject.GetComponent<SkeletonMove>().swapBody();
            //GameObject.FindGameObjectWithTag("PlayerSkeletonFull").GetComponent<SkeletonMove>().swapBody();
            //FindObjectOfType<SkeletonMove>().swapBody();

            arm.GetComponent<ArmMove>().setWalls(100);
            arm.GetComponent<ArmMove>().stopClimb();
            Destroy(arm, 0.01f);
        }
    }

    public void TakeRightArmControl()
    {
        if (GameObject.FindGameObjectWithTag("RightArm") != null)
        {
            Debug.Log("Taking control (RIGHT ARM)");

            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScrpit>().changeFollower(controledArm);
            //FindObjectOfType<CameraScrpit>().changeFollower(controledArm);

            controledArm.GetComponent<ArmMove>().enabled = true;

            if (globalSkelonMode == 30)
            {
                SkeletonPlayer.GetComponent<Animator>().gameObject.SetActive(false);
                SkeletonPlayer.GetComponent<Animator>().gameObject.SetActive(true);

                //SkeletonPlayer.GetComponent<CharacterController>().Move(new Vector3(0, -3.0f, 0)); ;
                SkeletonPlayer.GetComponent<SkeletonMove>().enabled = false;
                SkeletonPlayer.GetComponent<SkeletonGravity>().enabled = true;
            }
        }
    }



    public void BackSkeletonControl()
    {
        Debug.Log("BACK TO BODY");
        if (globalSkelonMode == 30)
        {
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraScrpit>().changeFollower(SkeletonPlayer);
            //FindObjectOfType<CameraScrpit>().changeFollower(SkeletonPlayer);
            SkeletonPlayer.GetComponent<SkeletonGravity>().enabled = false;
            SkeletonPlayer.GetComponent<SkeletonMove>().enabled = true;

            controledArm.GetComponent<Animator>().SetInteger("Move", 0);
            controledArm.GetComponent<ArmMove>().enabled = false;
        }
    }
}
