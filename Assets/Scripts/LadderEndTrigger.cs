﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderEndTrigger : MonoBehaviour
{
    public float angle = 90;
    public bool canIexit = true;
    public int dir = 1;

    public GameObject skeleton = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("PlayerSkeletonFull"))
        {
            if (skeleton != null)
            {
                //if (other.GetComponent<SkeletonMove>().getLadderMode())
                if (skeleton.GetComponent<SkeletonMove>().getLadderMode())
                {
                    if (canIexit == true)
                    {
                        canIexit = false;
                        //other.GetComponent<SkeletonMove>().climbEndTrigger(1, angle, dir);
                        skeleton.GetComponent<SkeletonMove>().climbEndTrigger(1, angle, dir);
                        canIexit = false;
                    }
                }
                else
                {
                    if (Input.GetKeyDown(KeyCode.C))
                    {
                        canIexit = false;
                        //other.GetComponent<SkeletonMove>().climbEndTrigger(2, angle, dir);
                        skeleton.GetComponent<SkeletonMove>().climbEndTrigger(2, angle, dir);
                        canIexit = false;
                    }
                }
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag.Equals("PlayerSkeletonFull"))
        {
            if (skeleton != null)
            {
                //if (other.GetComponent<SkeletonMove>().getLadderMode() == false)
                if (skeleton.GetComponent<SkeletonMove>().getLadderMode() == false)
                {
                    if (Input.GetKeyDown(KeyCode.C))
                    {
                        canIexit = false;
                        //other.GetComponent<SkeletonMove>().climbEndTrigger(2, angle, dir);
                        skeleton.GetComponent<SkeletonMove>().climbEndTrigger(2, angle, dir);
                        canIexit = false;
                    }
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("PlayerSkeletonFull"))
        {
            canIexit = true;
        }
    }
}
