﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKiller : MonoBehaviour
{
    public GameObject destroyedVersion = null;

    private List<GameObject> listOfBOnes = new List<GameObject>();


    private void OnCollisionEnter(Collision collision)
    {

        if ((collision.gameObject.tag == "PlayerSkeletonFull") || (collision.gameObject.tag == "PlayerSkull") || (collision.gameObject.tag == "RightArm"))
        {
            if ((collision.gameObject.tag == "PlayerSkeletonFull") && (destroyedVersion != null))
            {
                listOfBOnes.Add((GameObject)Instantiate(destroyedVersion, transform.position, transform.rotation));
            }

            if (listOfBOnes.Count > 2)
            {
                Destroy(listOfBOnes[0]);
                Destroy(listOfBOnes[1]);
                listOfBOnes.Remove(listOfBOnes[0]);
                listOfBOnes.Remove(listOfBOnes[0]);
            }

            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if ((other.tag == "PlayerSkeletonFull") || (other.tag == "PlayerSkull") || (other.tag == "RightArm"))
        {
            if ((other.gameObject.tag == "PlayerSkeletonFull") && (destroyedVersion != null))
            {
                listOfBOnes.Add((GameObject)Instantiate(destroyedVersion, transform.position, transform.rotation));
            }

            if (listOfBOnes.Count > 2)
            {
                Destroy(listOfBOnes[0]);
                Destroy(listOfBOnes[1]);
                listOfBOnes.Remove(listOfBOnes[0]);
                listOfBOnes.Remove(listOfBOnes[0]);
            }
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
        }
    }
}