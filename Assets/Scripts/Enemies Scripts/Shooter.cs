﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

	public GameObject bullet;
	public GameObject donor;
	void Start () {


	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButton (1)) {
			GameObject go = Instantiate (bullet, donor.transform.position, Quaternion.identity);
			Vector3 latestPos = go.transform.position;
			latestPos.y += 1;
			go.transform.position = latestPos;

		}
	}
}
