﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.IO;



public class EnemyScript : MonoBehaviour {
	public Canvas GUICanvas;
	public GameObject menu;


	void OnTriggerEnter(Collider player) 
	{
		if(player.tag == "SKELETON")
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex+1);
	}

	void OnMouseDown(){
		Time.timeScale = 0;
		GUICanvas.gameObject.SetActive(true);
	}

	public void Give(){
		Debug.Log ("give");

		Destroy(GameObject.FindGameObjectWithTag("destroySkull"));
		Destroy(GameObject.FindGameObjectWithTag("Enemy"));
		GUICanvas.gameObject.SetActive(false);
		Time.timeScale = 1;
	}

	public void Run(){
		Debug.Log ("run");
		GUICanvas.gameObject.SetActive(false);
		Time.timeScale = 1;
	}

}

