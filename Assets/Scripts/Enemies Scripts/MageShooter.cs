﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MageShooter : MonoBehaviour {
	public Transform player;
	public GameObject bullet;
	public Transform fireStarter;
	private float fireRate = 0.4f;
	public float fireCountdown = 0f;
	Vector3 firstPos;


    public AudioSource sounds = null;
    public AudioClip ballSound = null;
    public float audio_volume = 0.5f;
    public bool soundActive = false;

    void Start(){
		firstPos = fireStarter.transform.position;
	}

	void Update(){
		Vector3 lookAtPosition = player.position;
		lookAtPosition.y = transform.position.y;
		transform.LookAt(lookAtPosition);
		/*zależnie od używanych osi*/
		//if (Mathf.Abs(player.transform.position.x - transform.position.x) < 100.0f) {\
		if(Mathf.Abs(firstPos.y-fireStarter.position.y)<3.0f){
			if (fireCountdown <= 0f) {
				Shoot ();
				fireCountdown = 1f / fireRate;
			}
			fireCountdown -= Time.deltaTime;}
		//}
	}
	void Shoot(){
		GameObject bulletActivator = (GameObject)Instantiate (bullet, fireStarter.position, fireStarter.rotation);
		MageBullet mbullet = bulletActivator.GetComponent<MageBullet> ();


        if ((sounds != null) && (ballSound != null) && (soundActive))
        {
            sounds.PlayOneShot(ballSound, audio_volume);
        }


		if (mbullet != null)
			mbullet.Seek (player,transform);
	}


}
