﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MageBullet : MonoBehaviour {

	private Transform target;
	private  Transform witch;

	public void Seek(Transform _target,Transform witch)
	{
		target = _target;
		this.witch = witch;
	}
	void Update () {
		
		if (target == null) {
			Destroy (gameObject);
			return;
		}

		Vector3 dir = target.position - transform.position;
		float distance = 3f * Time.deltaTime;
		if (dir.magnitude <= distance) {
			Destroy (gameObject);
		}
		if (Mathf.Abs(transform.position.x- witch.transform.position.x)<400f) {
			transform.Translate (dir.normalized * distance, Space.World);
		} else
			Destroy (gameObject);
	}

	void Reload(){
		SceneManager.LoadScene(SceneManager.GetActiveScene ().buildIndex);
	}

	void OnTriggerEnter(Collider player) 
	{

		if (player.tag == "PlayerSkeletonFull")
        {
            //SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
            FindObjectOfType<GameManager>().EndGame();
        }
	}
	void HitPlayer (){
		//canvas.gameObject.SetActive(true);


		FindObjectOfType<GameManager>().EndGame();
		Invoke ("Reload", 0.8f);
		//Destroy (gameObject);  //można użyć do robienia ciosów -- hp playera
	}


}
