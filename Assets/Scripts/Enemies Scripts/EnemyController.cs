﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;


public class EnemyController : MonoBehaviour
{

    public GameObject teleport;
    public float distanceDown;
    public int jumpLoc;
    float radius = 15.0f;
    [SerializeField]
    Transform skeleton;
    NavMeshAgent agent;
    static Animator anim;
    float firstY, firstSkelY;


    //public AudioSource audios = null;
   // public AudioClip EnemySound = null;
   // public float audio_volume = 0.5f;

    bool check = false;

    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        firstY = transform.position.y;
        firstSkelY = skeleton.transform.position.y;

    }

    void Update()
    {
        Vector3 vec = skeleton.transform.position;
        if (skeleton.transform.position.x < jumpLoc)
            agent.SetDestination(vec);
        else
            Invoke("GetBack", 1.0f);

        //if ((audios != null) && (EnemySound != null))
          //  audios.PlayOneShot(EnemySound, audio_volume);
        //audios.
    }

    private void ChangeSpeedBack()
    {
        GetComponent<NavMeshAgent>().speed = 60.0f;//WAŻNE DO SPADKU
    }

    private void GetBack()
    {
        Vector3 vec = skeleton.transform.position;
        agent.SetDestination(vec);
    }

    //IEnumerator StopPlayer(Collider other)
    //{
    //    if (check == false)
    //    {
    //        check = true;
    //        FindObjectOfType<GameManager>().EndGame();
    //        yield return new WaitForSeconds(2);
    //        other.gameObject.transform.position = teleport.transform.position;
    //        check = false;
    //        //itemThrownToIgnite = false;
    //        //Destroy(gameObject);

    //    }
    //}


    void OnTriggerEnter(Collider player)
    {
        if (player.tag == "PlayerSkeletonFull")
        {
            //FindObjectOfType<GameManager> ().EndGame ();
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            //SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
        }

        if (player.tag == "RightArm")
        {
            //FindObjectOfType<GameManager> ().EndGame ();
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            //SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
        }

        if (player.tag == "PlayerSkull")
        {
            //FindObjectOfType<GameManager> ().EndGame ();
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            //SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
        }
    }

    //private void OnCollisionEnter(Collision player)
    //{
    //    if (player.gameObject.tag == "PlayerSkeletonFull")
    //    {
    //        //FindObjectOfType<GameManager> ().EndGame ();
    //        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
    //        //SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
    //    }

    //    if (player.gameObjecta.tag == "RightArm")
    //    {
    //        //FindObjectOfType<GameManager> ().EndGame ();
    //        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
    //        //SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
    //    }

    //    if (player.gameObject.tag == "PlayerSkull")
    //    {
    //        //FindObjectOfType<GameManager> ().EndGame ();
    //        GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
    //        //SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
    //    }
    //}

}
