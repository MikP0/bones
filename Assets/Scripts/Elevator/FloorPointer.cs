﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorPointer : MonoBehaviour {

    public int startFloor = 0;
    public float distance = 8.0f;
    public float lerpTime = 3;
    public int numberOfFloors = 3;
    private float elevatorDirection = 1f;

    public HashSet<GameObject> Passengers { get; set; }

    void Start()
    {
        Passengers = new HashSet<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "Torch") { 
            Rigidbody rigidbody = other.gameObject.GetComponent<Rigidbody>();
            //if(rigidbody)
            //    rigidbody.useGravity = false;
            Passengers.Add(other.gameObject);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "Torch")
        {
            Rigidbody rigidbody = other.gameObject.GetComponent<Rigidbody>();
            //if (rigidbody)
            //    rigidbody.useGravity = true;
            Passengers.Remove(other.gameObject);
        }
    }
    public int Floor
    {
        get
        {
            return startFloor;
        }

        set
        {
            startFloor = value;
        }
    }

    public float ElevatorDirection
    {
        get
        {
            return elevatorDirection;
        }

        set
        {
            elevatorDirection = value;
        }
    }

    public int NumberOfFloors
    {
        get
        {
            return numberOfFloors;
        }

        private set
        {
            numberOfFloors = value;
        }
    }

    public float Distance
    {
        get
        {
            return distance;
        }

        private set
        {
            distance = value;
        }
    }

    public float LerpTime
    {
        get
        {
            return lerpTime;
        }

        private set
        {
            lerpTime = value;
        }
    }

    

}
