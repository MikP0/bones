﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverManager : MonoBehaviour
{

    Animator anim;
    public GameObject elevator;
    public GameObject elevatorPlatform;
    public int leverFloor = -1; // on which floor the lever stands ( -1 => lever on elevator platform )

    public static bool PlayerOnElevatorCaller;
    public static bool PlayerOnElevator;
    private Vector3 startPos;
    private Vector3 endPos;
    private float currentLerpTime = 0;
    private bool keyPressed = false;
    private static bool leverEnabled = true;
    private FloorPointer floorPointer;
    private float lerpTimeWhenElevatorIsCalled;
    private int difference;
    private CameraScrpit cameraScript;
    private List<GameObject> passengersList;

    //do sprawdzenia czy sterujemy szkieletem gdy winda rusza
    private bool skeletonMovementState;

    void Start()
    {
        anim = GetComponentInParent<Animator>();
        //currentFloor = startFloor;
        floorPointer = elevatorPlatform.GetComponent<FloorPointer>();
        lerpTimeWhenElevatorIsCalled = floorPointer.LerpTime;
        cameraScript = GameObject.Find("Main Camera").gameObject.GetComponent<CameraScrpit>();
    }

    void Update()
    {
        if (keyPressed)
        {
            
            currentLerpTime += Time.deltaTime;
            
            float percentage = currentLerpTime / lerpTimeWhenElevatorIsCalled;
            elevator.transform.position = Vector3.Lerp(startPos, endPos, percentage);

            foreach (GameObject passenger in floorPointer.Passengers)
            {
                if (passenger)
                {
                    Vector3 passengerStartPos = new Vector3(startPos.x, startPos.y, passenger.transform.position.z);
                    Vector3 passengerEndPos = new Vector3(endPos.x, endPos.y, passenger.transform.position.z);
                    passenger.transform.position = Vector3.Lerp(passengerStartPos, passengerEndPos, percentage);
                }
            }

            if (currentLerpTime >= lerpTimeWhenElevatorIsCalled)
            {
                currentLerpTime = lerpTimeWhenElevatorIsCalled;
                if (leverFloor == -1)
                {
                    floorPointer.Floor += (int)floorPointer.ElevatorDirection;
                }
                else
                {
                    floorPointer.Floor += difference;
                }
                //Debug.Log(floorPointer.Floor);
                EnablePassengersMovement();
                keyPressed = false;
                leverEnabled = true;
            }
        }
    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "PlayerSkeletonFull" || other.gameObject.tag == "RightArm")
        {
            if (Input.GetKey("f") && WhatIsCameraFollowing(other))
            {
                anim.SetBool("isLeverUsed", true);
                if (leverEnabled)
                {
                    AddPassengersToList();
                    DisablePassengersMovement();
                    leverEnabled = false;
                    LeverIsUsed();
                }
            }
            
        }

        if (WhatIsCameraFollowing(other))
        {
            if (leverFloor == -1)
            {
                PlayerOnElevator = true;
                PlayerOnElevatorCaller = false;
            }
            else
            {
                PlayerOnElevatorCaller = true;
                PlayerOnElevator = false;
            }
        }

        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            ObjectToHolding.lowerUsagePriority = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "PlayerSkeletonFull" || other.gameObject.tag == "RightArm")
        {
            if (leverFloor == -1)
            {
                PlayerOnElevator = false;
            }
            else
            {
                PlayerOnElevatorCaller = false;
            }

        }

        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            ObjectToHolding.lowerUsagePriority = false;
        }
    }

    bool CheckIfItIsElevator()
    {
        if (leverFloor == -1)
            return true;
        else
            return false;
    }

    public void LeverIsUsed()
    {
        CheckDirection();
        currentLerpTime = 0;
        CalculatePath();
    }
    void CheckDirection()
    {
        if(floorPointer.Floor == 0)
        {
            floorPointer.ElevatorDirection = 1.0f;
        }
        else if (floorPointer.Floor == floorPointer.NumberOfFloors)
        {
            floorPointer.ElevatorDirection = -1.0f;
        }
    }

    void CalculatePath()
    {
        startPos = elevator.transform.position;
        if (leverFloor == -1)
        {
            lerpTimeWhenElevatorIsCalled = floorPointer.LerpTime;
            endPos = elevator.transform.position + Vector3.up * floorPointer.Distance * floorPointer.ElevatorDirection;
            keyPressed = true;
        }
        else
        {
            difference = leverFloor - floorPointer.Floor;
            int absDifference = Mathf.Abs(difference);
            float distance = floorPointer.Distance * absDifference;
            lerpTimeWhenElevatorIsCalled = floorPointer.LerpTime * absDifference;
            if (difference < 0)
            {
                floorPointer.ElevatorDirection = -1f;
                endPos = elevator.transform.position + Vector3.up * distance * floorPointer.ElevatorDirection;
            }
            else if (difference > 0)
            {
                floorPointer.ElevatorDirection = 1f;
                endPos = elevator.transform.position + Vector3.up * distance * floorPointer.ElevatorDirection;
            }

            if (difference == 0)
            {
                keyPressed = false;
                leverEnabled = true;
            }
            else 
            {
                keyPressed = true;
            }
        }
        //Debug.Log(endPos);
    }

    bool WhatIsCameraFollowing(Collider other)
    {
        //Debug.Log(cameraScript.target.tag);
        if (other.gameObject.tag == cameraScript.target.tag)
            return true;
        else
            return false;
    }

    void AddPassengersToList()
    {
        passengersList = new List<GameObject>();
        foreach (GameObject passenger in floorPointer.Passengers)
        {
            passengersList.Add(passenger);
        }
    }

    void DisablePassengersMovement()
    {
        foreach (GameObject passenger in passengersList)
        {
            if (passenger)
            {
                if (passenger.tag == "PlayerSkeletonFull")
                {
                    SkeletonMove skeletonMove = passenger.GetComponent<SkeletonMove>();
                    if (skeletonMove)
                    {
                        skeletonMovementState = skeletonMove.enabled;
                        if (skeletonMovementState)
                            skeletonMove.enabled = false;
                    }
                }
                Rigidbody passengerRigidbody = passenger.GetComponent<Rigidbody>();
                if (passengerRigidbody)
                {
                    passengerRigidbody.useGravity = false;
                }
            }
        }
    }

    void EnablePassengersMovement()
    {
        foreach (GameObject passenger in passengersList)
        {
            if (passenger)
            {
                if (passenger.tag == "PlayerSkeletonFull")
                {
                    SkeletonMove skeletonMove = passenger.GetComponent<SkeletonMove>();
                    if (skeletonMove)
                    {
                        if (skeletonMovementState)
                            skeletonMove.enabled = true;
                    }
                }
                Rigidbody passengerRigidbody = passenger.GetComponent<Rigidbody>();
                if (passengerRigidbody)
                {
                    passengerRigidbody.useGravity = true;
                }
            }
        }
    }
}
