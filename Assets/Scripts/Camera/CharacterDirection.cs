﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterDirection : MonoBehaviour
{
    public GameObject Camera;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("PlayerSkeletonFull"))
        {
            if ((Camera.GetComponent<CameraScrpit>().stateBefore == 1))
            {
                other.gameObject.GetComponent<SkeletonMove>().rotations(2);
            }
            else if ((Camera.GetComponent<CameraScrpit>().stateBefore == 2))
            {
                other.gameObject.GetComponent<SkeletonMove>().rotations(1);
            }
        }
        else if (other.gameObject.tag.Equals("PlayerSkull"))
        {

        }
        else if (other.gameObject.tag.Equals("RightArm"))
        {

        }
    }
}
