﻿using UnityEngine;

public class LadderTrigger : MonoBehaviour
{
    public GameObject Camera;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("PlayerSkeletonFull")) 
        {
            if (Camera.GetComponent<CameraScrpit>().cameraDirection == 1)
            {
                Camera.GetComponent<CameraScrpit>().setRotations(5);
            }
            else if (Camera.GetComponent<CameraScrpit>().cameraDirection == 5)
            {
                Camera.GetComponent<CameraScrpit>().setRotations(55);
            }

        }
    }
}