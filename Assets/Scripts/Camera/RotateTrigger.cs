﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTrigger : MonoBehaviour
{
    public GameObject Camera;
    public int mode = 1;

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.tag.Equals("PlayerSkeletonFull")) || (other.gameObject.tag.Equals("PlayerSkull")) || (other.gameObject.tag.Equals("RightArm")))
        {
            if (mode == 1)
            {
                if (Camera.GetComponent<CameraScrpit>().cameraDirection == 2)
                {
                    Camera.GetComponent<CameraScrpit>().setRotations(2);
                }
                else if (Camera.GetComponent<CameraScrpit>().cameraDirection == 1)
                {
                    Camera.GetComponent<CameraScrpit>().setRotations(1);
                }
            }
            else if (mode == 2)
            {
                if (Camera.GetComponent<CameraScrpit>().cameraDirection == 1)
                {
                    Camera.GetComponent<CameraScrpit>().setRotations(3);
                }
                else if (Camera.GetComponent<CameraScrpit>().cameraDirection == 3)
                {
                    Camera.GetComponent<CameraScrpit>().setRotations(1);
                }
            }
        }
        //else if (other.gameObject.tag.Equals("PlayerSkull"))
        //{
        //    if (mode == 1)
        //    {
        //        if (Camera.GetComponent<CameraScrpit>().cameraDirection == 2)
        //        {
        //            Camera.GetComponent<CameraScrpit>().setRotations(2);

        //        }
        //        else if (Camera.GetComponent<CameraScrpit>().cameraDirection == 1)
        //        {
        //            Camera.GetComponent<CameraScrpit>().setRotations(1);
        //        }
        //    }
        //}
        //else if (other.gameObject.tag.Equals("RightArm"))
        //{
        //    if (mode == 1)
        //    {
        //        if (Camera.GetComponent<CameraScrpit>().cameraDirection == 2)
        //        {
        //            Camera.GetComponent<CameraScrpit>().setRotations(2);

        //        }
        //        else if (Camera.GetComponent<CameraScrpit>().cameraDirection == 1)
        //        {
        //            Camera.GetComponent<CameraScrpit>().setRotations(1);
        //        }
        //    }
        //}
    }
}
