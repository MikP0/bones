﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
{
    public float speedRotation = 0.0f;

    bool rotation = false;
    public int dir = 0;

   // float startRotationY;

    //private void Start()
    //{
    //    startRotationY = transform.rotation.y;
    //}


    // Update is called once per frame
    void Update ()
    {
        //Debug.Log(transform.eulerAngles.y);
        if (rotation == true)
        {
            if ((transform.rotation.y >= -0.70f) && (transform.rotation.y <= 0.70f))
                transform.Rotate(0, speedRotation * Time.deltaTime, 0);
            else
                rotation = false;
        }
    }

    public void rotate( int dirr)
    {
        //rotation = check;
        rotation = true;

        if (dirr == 1)
        {
            speedRotation = -50.0f;
            dir = dirr;
        }
        else if (dirr == 2)
        {
            speedRotation = 50.0f;
            dir = dirr;
        }
    }
}
