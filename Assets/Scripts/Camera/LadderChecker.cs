﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderChecker : MonoBehaviour
{
    public GameObject Camera;
    public Vector3 offsetCheck;
    public int camDir;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("PlayerSkeletonFull"))
        {
            Camera.GetComponent<CameraScrpit>().offset = offsetCheck;
            Camera.GetComponent<CameraScrpit>().setCamDir(camDir);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("PlayerSkeletonFull"))
        {
            Camera.GetComponent<CameraScrpit>().offset = offsetCheck;
            Camera.GetComponent<CameraScrpit>().setCamDir(camDir);
        }
    }
}
