﻿using UnityEngine;

public class CameraScrpit : MonoBehaviour
{
    public int cameraDirection { get; set; }
    //1 - x direction
    //2 - z direction
    //3 -?

    public GameObject target;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    private Vector3 offsetSec;
    Vector3 desiredPosition;
    Vector3 smoothedPosition;

    private Vector3 remOffset;
    private Vector3 remOffsetSec;


    bool rotCameraT = false;
    bool remrotCamera;

    public float cameraShake = 1.0f;

    int rotationsWhichOne = 0;
    public int stateBefore { get; set; }


    float tempStateX = 0.0f;
    float tempStateY = 0.0f;
    float tempStateZ = 0.0f;
    public Vector3 maxLookAr = new Vector3(10.0f, 15.0f, -10.0f);
    public Vector3 minLookAr = new Vector3(-10.0f, -5.0f, -40.0f);


    private int cameraSet = 0;


    private Vector3 defPos;
    private Vector3 defPosSec;


    // Use this for initialization
    void Start()
    {
        offsetSec = offset / 2.0f;

        remrotCamera = rotCameraT;
        cameraDirection = 1;
        stateBefore = 100;

        remOffset = offset;
        remOffsetSec = offsetSec;

        defPos = remOffset;
        defPosSec = remOffsetSec;

        Cursor.visible = false;
    }

    public void changeCameraSet()
    {
        if (cameraSet == 0)
        {
            cameraSet = 1;

            remOffset.z = minLookAr.z;
            remOffsetSec.z = minLookAr.z;
        }
        else if (cameraSet == 1)
        {
            cameraSet = 0;

            remOffset.z = defPos.z;
            remOffsetSec.z = defPosSec.z;
        }
    }


    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            tempStateX = (-1) * Input.GetAxis("Mouse X");
            tempStateY = (-1) * Input.GetAxis("Mouse Y");

            // Debug.Log(tempStateY);
        }

        if (Input.GetMouseButton(1))
        {
            tempStateZ = Input.GetAxis("Mouse Y");
        }


        if (Input.GetKeyDown(KeyCode.Tab))
        {
            changeCameraSet();
        }




        if ((tempStateX != 0) || (tempStateY != 0) || (tempStateZ != 0))
        {
            offset.x += tempStateX;
            offset.y += tempStateY;
            offset.z += tempStateZ;

            offsetSec.x += tempStateX;
            offsetSec.y += tempStateY;
            offsetSec.z += tempStateZ;

            tempStateX = 0;
            tempStateY = 0;
            tempStateZ = 0;

            if (offset.x > maxLookAr.x)
            {
                offset.x = maxLookAr.x;
            }
            else if (offset.x < minLookAr.x)
            {
                offset.x = minLookAr.x;
            }

            if (offset.y > maxLookAr.y)
            {
                offset.y = maxLookAr.y;
            }
            else if (offset.y < minLookAr.y)
            {
                offset.y = minLookAr.y;
            }

            if (offset.z > maxLookAr.z)
            {
                offset.z = maxLookAr.z;
            }
            else if (offset.z < minLookAr.z)
            {
                offset.z = minLookAr.z;
            }


            if (offsetSec.x > maxLookAr.x)
            {
                offsetSec.x = maxLookAr.x;
            }
            else if (offsetSec.x < minLookAr.x)
            {
                offsetSec.x = minLookAr.x;
            }

            if (offsetSec.y > maxLookAr.y)
            {
                offsetSec.y = maxLookAr.y;
            }
            else if (offsetSec.y < minLookAr.y)
            {
                offsetSec.y = minLookAr.y;
            }

            if (offsetSec.z > maxLookAr.z)
            {
                offsetSec.z = maxLookAr.z;
            }
            else if (offsetSec.z < minLookAr.z)
            {
                offsetSec.z = minLookAr.z;
            }

        }
        else
        {
            //if ((rotCamera == true) && (cameraDirection == 1))
            //    offset.x = (-1.0f) * Input.GetAxisRaw("Horizontal") * cameraShake;
            //else if ((rotCamera == true) && (cameraDirection == 2))
            //    offset.z = (-1.0f) * Input.GetAxisRaw("Horizontal") * cameraShake;


            if (offset.x < remOffset.x)
            {
                offset.x += 0.2f;
            }
            else if (offset.x > remOffset.x)
            {
                offset.x -= 0.2f;
            }

            if (offset.y < remOffset.y)
            {
                offset.y += 0.2f;
            }
            else if (offset.y > remOffset.y)
            {
                offset.y -= 0.2f;
            }


            if (offsetSec.x < remOffsetSec.x)
            {
                offsetSec.x += 0.2f;
            }
            else if (offsetSec.x > remOffsetSec.x)
            {
                offsetSec.x -= 0.2f;
            }

            if (offsetSec.y < remOffsetSec.y)
            {
                offsetSec.y += 0.2f;
            }
            else if (offsetSec.y > remOffsetSec.y)
            {
                offsetSec.y -= 0.2f;
            }


            if (!Input.GetMouseButton(1))
            {
                if (offset.z < remOffset.z)
                {
                    offset.z += 0.5f;
                }
                else if (offset.z > remOffset.z)
                {
                    offset.z -= 0.2f;
                }

                if (offsetSec.z < remOffsetSec.z)
                {
                    offsetSec.z += 0.5f;
                }
                else if (offsetSec.z > remOffsetSec.z)
                {
                    offsetSec.z -= 0.2f;
                }
            }

        }


    }

    private void LateUpdate()
    {

        Rotations();

        if (target.Equals(GameObject.FindGameObjectWithTag("PlayerSkull")))
        {
            desiredPosition = target.transform.position + offsetSec;
            smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
            transform.LookAt(target.transform);
        }
        else if (target.Equals(GameObject.FindGameObjectWithTag("RightArm")))
        {
            desiredPosition = target.transform.position + offsetSec;
            smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
            transform.LookAt(target.transform);
        }
        else if (target.Equals(GameObject.FindGameObjectWithTag("PlayerSkeletonFull")))
        {
            desiredPosition = target.transform.position + offset;
            smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
            transform.LookAt(target.transform);
        }
        else
        {
            desiredPosition = target.transform.position + offset;
            smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
            transform.LookAt(target.transform);
        }

    }

    public void setRotations(int whichOne)
    {
        stateBefore = cameraDirection;
        cameraDirection = 0;
        rotationsWhichOne = whichOne;
    }

    public void setCamDir(int dir)
    {
        cameraDirection = dir;
    }

    void Rotations()
    {
        // 1 - ext left
        // 2 - int right
        // 3 - int right sec

        switch (rotationsWhichOne)
        {
            case 1:
                {
                    rotCameraT = false;
                    offset.x = offset.x + 0.2f;
                    offset.z = offset.z + 0.2f;

                    if (offset.x > 20)
                    {
                        offset.x = 20;
                        offset.z = 0;
                        remOffset = offset;
                        rotationsWhichOne = 0;
                        rotCameraT = remrotCamera;
                        cameraDirection = 2;
                    }
                    break;
                }

            case 2:
                {
                    rotCameraT = false;
                    offset.x = offset.x - 0.2f;
                    offset.z = offset.z - 0.2f;

                    if (offset.x < 0)
                    {
                        offset.x = 0;
                        offset.z = -20;
                        remOffset = offset;
                        rotationsWhichOne = 0;
                        rotCameraT = remrotCamera;
                        cameraDirection = 1;
                    }
                    break;
                }

            case 3:
                {
                    rotCameraT = false;
                    offset.x = offset.x - 0.2f;
                    offset.z = offset.z + 0.2f;

                    if (offset.x < 0)
                    {
                        offset.x = -20;
                        offset.z = 0;
                        remOffset = offset;
                        rotationsWhichOne = 0;
                        rotCameraT = remrotCamera;
                        cameraDirection = 3;
                    }
                    break;
                }

            case 5:
                {
                    if (offset.z < 0)
                    {
                        offset.x = offset.x - 0.2f;
                    }
                    else
                    {
                        offset.x = offset.x + 0.2f;
                    }

                    rotCameraT = false;
                    // offset.x = offset.x - 0.2f;
                    offset.z = offset.z + 0.2f;


                    if (offset.z > 20)
                    {
                        offset.x = 0;
                        offset.z = 20;
                        rotationsWhichOne = 0;
                        rotCameraT = remrotCamera;
                        cameraDirection = 5;
                    }

                    remOffset = offset;

                    break;
                }

            case 55:
                {
                    if (offset.z > 0)
                    {
                        offset.x = offset.x - 0.2f;
                    }
                    else
                    {
                        offset.x = offset.x + 0.2f;
                    }

                    rotCameraT = false;
                    // offset.x = offset.x - 0.2f;
                    offset.z = offset.z - 0.2f;


                    if (offset.z < -20)
                    {
                        offset.x = 0;
                        offset.z = -20;
                        rotationsWhichOne = 0;
                        rotCameraT = remrotCamera;
                        cameraDirection = 1;
                    }

                    remOffset = offset;

                    break;
                }

            default:
                {
                    break;
                }
        }
    }

    public void changeFollower(GameObject gameObject)
    {
        target = gameObject;
    }
}
