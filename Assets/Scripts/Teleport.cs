﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport : MonoBehaviour {

    // Use this for initialization
    public GameObject ObjectToTeleport;
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "PlayerSkull")
        {
            ObjectToTeleport.transform.position = new Vector3(878.84f, 38.38f, -85.9f);
        }
    }
}
