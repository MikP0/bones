﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    public bool activated = false;
    public static GameObject[] CheckPointsList;

    public GameObject altar = null;

    // Use this for initialization
    void Start()
    {
        CheckPointsList = GameObject.FindGameObjectsWithTag("CheckPoint");
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void ActivateCheckPoint()
    {
        foreach (GameObject cp in CheckPointsList)
        {
            if (cp.GetComponent<CheckPoint>().altar != null)
            {
                cp.GetComponent<CheckPoint>().altar.GetComponent<CheckpointLight>().LightOff();
            }

            cp.GetComponent<CheckPoint>().activated = false;          
        }

        activated = true;

        if (altar != null)
        {
            altar.GetComponent<CheckpointLight>().LightOn();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerSkeletonFull")
        {
            ActivateCheckPoint();
        }
        else if (other.tag == "PlayerSkull")
        {
            ActivateCheckPoint();
        }
        else if (other.tag == "RightArm")
        {
            ActivateCheckPoint();
        }
    }

    public static Vector3 GetActiveCheckPointPosition()
    {
        Vector3 result = new Vector3(0, 0, 0);

        if (CheckPointsList != null)
        {
            foreach (GameObject cp in CheckPointsList)
            {
                if (cp.GetComponent<CheckPoint>().activated)
                {
                    result = cp.transform.position;
                    break;
                }
            }
        }

        return result;
    }
}

