﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossActivatorSound : MonoBehaviour
{
    public GameObject witch = null;

    private void OnTriggerEnter(Collider other)
    {
        if (witch != null)
        {
            witch.GetComponent<MageShooter>().soundActive = true;
        }
    }
}
