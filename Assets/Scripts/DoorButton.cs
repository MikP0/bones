﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorButton : MonoBehaviour {

    public GameObject door;
    public enum ButtonTarget { Everyone, FullSkeleton, Skull, Arm, Body, SkeletonBody };
    public ButtonTarget buttonTarget = ButtonTarget.Everyone;
    // public LayerMask m_LayerMask;
    // Animator anim;

    List<GameObject> colliderList = new List<GameObject>();


    void Start()
    {
        //anim = door.GetComponent<Animator>();
    }


    void FixedUpdate()
    {
        colliderList.Clear();
    }

    void Update()
    {
        var numberOfColliders = colliderList.Count;
    }
    void OnTriggerEnter(Collider other)
    {
        if (!colliderList.Contains(other.gameObject))
        {
            if (other.tag == "PlayerSkeletonFull" && (buttonTarget == ButtonTarget.Everyone || buttonTarget == ButtonTarget.FullSkeleton || buttonTarget == ButtonTarget.SkeletonBody))
            {
                colliderList.Add(other.gameObject);
                door.GetComponent<MoveDoor>().DoorDirA();
            }

            else if (other.tag == "PlayerSkull" && (buttonTarget == ButtonTarget.Everyone || buttonTarget == ButtonTarget.Skull))
            {
                colliderList.Add(other.gameObject);
                door.GetComponent<MoveDoor>().DoorDirA();
            }

            else if (other.tag == "RightArm" && (buttonTarget == ButtonTarget.Everyone || buttonTarget == ButtonTarget.Arm))
            {
                colliderList.Add(other.gameObject);
                door.GetComponent<MoveDoor>().DoorDirA();
            }
            else if (other.tag == "PartBody" && (buttonTarget == ButtonTarget.Everyone || buttonTarget == ButtonTarget.Body || buttonTarget == ButtonTarget.SkeletonBody))
            {
                colliderList.Add(other.gameObject);
                door.GetComponent<MoveDoor>().DoorDirA();
            }
        }
        //Debug.Log(colliderList.Count);
    }

    private void OnTriggerStay(Collider other)
    {
        OnTriggerEnter(other);
    }

    void OnTriggerExit(Collider other)
    {
        colliderList.Remove(other.gameObject);
        if (colliderList.Count == 0)
            door.GetComponent<MoveDoor>().DoorDirB();
        // anim.SetBool("isOpen", false);
    }
}
