﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour {

    public GameObject pickUpEffect;
    private GameObject instance;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            instance = Instantiate(pickUpEffect, transform.position - new Vector3(0, 0.5f, 0), transform.rotation);
            Destroy(instance, 2);
            KeyManager.isKeyPickedUp = true;
            Destroy(gameObject);
        }
        else if (other.gameObject.tag == "RightArm")
        {
            instance = Instantiate(pickUpEffect, transform.position - new Vector3(0, 0.5f, 0), transform.rotation);
            Destroy(instance, 2);
            KeyManager.isKeyPickedUp = true;
            Destroy(gameObject);
        }
    }
}
