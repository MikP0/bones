﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatformAct2 : MonoBehaviour
{

    public float movementSpeed = 10;
    public float EndPos;

    public float StartPos;
    public int direction = 0;

    private float waitTime = 0.0f;

    // Update is called once per frame
    void Update()
    {
        if (direction == 1)
        {
            if (transform.position.x < EndPos)
            {
                transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
            }
            else
            {
                if (waitTime <= 2.0f)
                {
                    waitTime += Time.deltaTime;
                }
                else
                {                   
                    direction = -1;
                    waitTime = 0.0f;
                }
            }
        }

        if (direction == -1)
        {
            if (transform.position.x > StartPos)
            {
                transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
            }
            else
            {
                if (waitTime <= 2.0f)
                {
                    waitTime += Time.deltaTime;
                }
                else
                {                   
                    direction = 1;
                    waitTime = 0.0f;
                }
            }
        }


        if (direction == 2)
        {
            if (transform.position.x < EndPos)
            {
                transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
            }
            else
            {
                if (waitTime <= 2.0f)
                {
                    waitTime += Time.deltaTime;
                }
                else
                {
                    direction = -1;
                    waitTime = 0.0f;
                }
            }
        }

        if (direction == -2)
        {
            if (transform.position.x > StartPos)
            {
                transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
            }
            else
            {
                if (waitTime <= 2.0f)
                {
                    waitTime += Time.deltaTime;
                }
                else
                {
                    direction = 1;
                    waitTime = 0.0f;
                }
            }
        }
    }
}
