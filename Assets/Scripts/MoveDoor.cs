﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveDoor : MonoBehaviour {

    public float movementSpeed = 10;
    public float EndPos;
   // public GameObject player;

    public float StartPos;

    public int direction = 0;
    public bool isLadder = false;
    public bool isFloatingPlatform = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (direction == 1)
        {
            if (transform.position.z < EndPos)
            {
                transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
            }
        }

        if (direction == -1)
        {
            if (transform.position.z > StartPos)
            {
                transform.Translate(Vector3.back * movementSpeed * Time.deltaTime);
            }
        }

        if (direction == 2)
        {
            if (transform.position.y > EndPos)
            {
                transform.Translate(Vector3.down * movementSpeed * Time.deltaTime);
            }
        }

        if (direction == -2)
        {
            if (transform.position.y < StartPos)
            {
                transform.Translate(Vector3.up * movementSpeed * 3.0f * Time.deltaTime);
            }
        }

        if (direction == 3)
        {
            if (transform.position.x > EndPos)
            {
                transform.Translate(Vector3.left * movementSpeed * Time.deltaTime);
            }
        }

        if (direction == -3)
        {
            if (transform.position.x < StartPos)
            {
                transform.Translate(Vector3.right * movementSpeed * Time.deltaTime);
            }
        }
    }

    public void DoorDirA()
    {
        if ((!isLadder) && (!isFloatingPlatform))
        {
            direction = 1;
        }
        else if ((isLadder) && (!isFloatingPlatform))
        {
            direction = 2;
        }
        else if ((!isLadder) && (isFloatingPlatform))
        {
            direction = 3;
        }
    }

    public void DoorDirB()
    {
        if ((!isLadder)&&(!isFloatingPlatform))
        {
            direction = -1;
        }
        else if ((isLadder)&& (!isFloatingPlatform))
        {
            direction = -2;
        }
        else if ((!isLadder) && (isFloatingPlatform))
        {
            direction = -3;
        }
    }
}
