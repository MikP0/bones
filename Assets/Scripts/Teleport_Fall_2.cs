﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleport_Fall_2 : MonoBehaviour
{

    public GameObject teleportEnD;

    private float time = 0.0f;

    void OnTriggerEnter(Collider other)
    {
        other.transform.parent.transform.position = teleportEnD.transform.position;
    }
}
