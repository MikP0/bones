﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayer : MonoBehaviour {

	public GameObject Teleport;
    public GameObject ballPositon;

    bool check = false;

    private void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
	{

        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            //FindObjectOfType<GameManager>().EndGame();
            //other.gameObject.GetComponent<SkeletonMove>().diedAnimation();
           // GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            //FindObjectOfType<SkeletonMove>().diedAnimation();

            StartCoroutine(StopPlayer());
        }
        else if (other.gameObject.tag == "PlayerSkull")
        {
            //FindObjectOfType<GameManager>().EndGame();
           // GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            StartCoroutine(StopPlayer());
        }
        else if (other.gameObject.tag == "RightArm")
        {
            //FindObjectOfType<GameManager>().EndGame();
            //GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            StartCoroutine(StopPlayer());
        }
    }

    IEnumerator StopPlayer()
    {
        if (check == false)
        {
            check = true;
            
            yield return new WaitForSeconds(2);
            transform.position = ballPositon.transform.position;
            GetComponent<Rigidbody>().isKinematic = true;
            //other.gameObject.transform.position = Teleport.transform.position;
            check = false;
        }
    }
}
