﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointRegisterToMenu : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject checkpoint;
    void OnTriggerEnter(Collider player)
    {
        if (player.tag == "PlayerSkeletonFull")
        {
            pauseMenu.GetComponent<PauseMenuScript>().checkpoint= checkpoint;
        }
    }
}
