﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    bool gameHasEnded = false;
    public float restartDelay = 10.0f;
    public GameObject gameOverUI;
    public AudioSource audios;
    public AudioClip deathSound;
    public float volume;

    public GameObject skeleton = null;
    public GameObject skull = null;


    public void EndGame()
    {
        if (!gameHasEnded)
        {
            audios.PlayOneShot(deathSound, volume);
            gameHasEnded = true;
            GameOver(true);

            if ((skeleton != null) && (skeleton.active))
            {
                skeleton.SetActive(false);
                Invoke("setActUnIve", 1.0f);
            }
            //else if ((skull != null) && (skull.active))
            //{
            //    skull.SetActive(false);
            //}
            //Invoke("setActUnIve", 1.0f);

            Invoke("Restart", restartDelay);
        }
    }

    public void GameOver(bool check)
    {
        gameOverUI.SetActive(check);
    }

    public void setActUnIve()
    {
        if ((skeleton != null))
        {
            skeleton.SetActive(true);
        }
        //else if ((skull != null))
        //{
        //    skull.SetActive(true);
        //}
    }


    void Restart()
    {

        //if (FindObjectOfType<SkeletonMove>() != null && FindObjectOfType<ArmMove>() == null)
        //    FindObjectOfType<SkeletonMove>().transform.position = CheckPoint.GetActiveCheckPointPosition();
        //else if (FindObjectOfType<SkullMove>() != null)
        //{
        //    //FindObjectOfType<SkullMove>().transform.position = CheckPoint.GetActiveCheckPointPosition();
        //    GameObject lastbody = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().getLastBody();
        //    lastbody.transform.position = CheckPoint.GetActiveCheckPointPosition();
        //    FindObjectOfType<SkullMove>().transform.position = new Vector3(CheckPoint.GetActiveCheckPointPosition().x, CheckPoint.GetActiveCheckPointPosition().y + 1.5f, CheckPoint.GetActiveCheckPointPosition().z);            
        //}
        //else if (FindObjectOfType<ArmMove>() != null && FindObjectOfType<SkeletonMove>() != null)
        //{
        //    FindObjectOfType<ArmMove>().transform.position = CheckPoint.GetActiveCheckPointPosition();
        //    FindObjectOfType<SkeletonMove>().transform.position = CheckPoint.GetActiveCheckPointPosition();
        //}



        if (skeleton.active && FindObjectOfType<ArmMove>() == null)
        {
            //skeleton.transform.position = CheckPoint.GetActiveCheckPointPosition();
            skeleton.transform.position = new Vector3(CheckPoint.GetActiveCheckPointPosition().x, CheckPoint.GetActiveCheckPointPosition().y, skeleton.transform.position.z);
        }
        else if (skull.active)
        {
            GameObject lastbody = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().getLastBody();
            lastbody.transform.position = CheckPoint.GetActiveCheckPointPosition();
            skull.transform.position = new Vector3(CheckPoint.GetActiveCheckPointPosition().x, CheckPoint.GetActiveCheckPointPosition().y + 1.5f, CheckPoint.GetActiveCheckPointPosition().z);
        }
        else if (FindObjectOfType<ArmMove>() != null && skeleton.active)
        {
            //FindObjectOfType<ArmMove>().transform.position = new Vector3(CheckPoint.GetActiveCheckPointPosition().x + 0.3f, CheckPoint.GetActiveCheckPointPosition().y + 0.5f, CheckPoint.GetActiveCheckPointPosition().z + 0.3f);
            //skeleton.transform.position = CheckPoint.GetActiveCheckPointPosition();
            skeleton.transform.position = new Vector3(CheckPoint.GetActiveCheckPointPosition().x, CheckPoint.GetActiveCheckPointPosition().y, skeleton.transform.position.z);
            FindObjectOfType<ArmMove>().transform.position = new Vector3(skeleton.transform.position.x + 0.3f, skeleton.transform.position.y, skeleton.transform.position.z + 0.3f);
        }






        GameOver(false);
        gameHasEnded = false;
    }
}
