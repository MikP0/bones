﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIContentManager : MonoBehaviour {

    public GameObject canvas;

    GameObject[] arms;
    //GameObject[] rightArm;
    GameObject closest;
    float distance;
    float curDistance;
    Vector3 position;
    Vector3 diff;
    public static bool pickUp = false;

    private void Update()
    {
        arms = GameObject.FindGameObjectsWithTag("RightArm");
        
        closest = null;
        distance = Mathf.Infinity;
        position = transform.position;


        foreach (GameObject go in arms)
        {
            diff = go.transform.position - position;
            curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        if ((closest != null) && (Vector3.Distance(transform.position, closest.transform.position) < 1.0f))
        {
            canvas.transform.GetChild(1).gameObject.SetActive(true);
            pickUp = true;
        }
        else
        {
            canvas.transform.GetChild(1).gameObject.SetActive(false);
            pickUp = false;
        }

        if(KeyManager.isKeyPickedUp)
        {
            canvas.transform.Find("KeyIcon").gameObject.SetActive(true);
        }
        else
        {
            canvas.transform.Find("KeyIcon").gameObject.SetActive(false);
        }
    }

    private void OnDisable()
    {
       // GameObject armSprite = canvas.transform.GetChild(1).gameObject;
       // if (armSprite )
        //    armSprite.SetActive(false);
    }

}
