﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderCollision : MonoBehaviour
{
    public float sideRot = 0.7071068f;
    // -0.7071068
    public static bool playerInRange = false;

    public GameObject skeleton = null;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("PlayerSkeletonFull"))
        {           
            if (skeleton != null)
                skeleton.GetComponent<SkeletonMove>().setLadderClimbAble(true, sideRot);
                //other.GetComponent<SkeletonMove>().setLadderClimbAble(true, sideRot);

            ObjectToHolding.lowerUsagePriority = true; //nie mozna upuszczać np. pochodni na drabinie
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("PlayerSkeletonFull"))
        {
            if (skeleton != null)
                skeleton.GetComponent<SkeletonMove>().setLadderClimbAble(false, 0.0f);
                //other.GetComponent<SkeletonMove>().setLadderClimbAble(false, 0.0f);

            ObjectToHolding.lowerUsagePriority = false;
            playerInRange = false;
        }
    }
}
