﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arsoning : MonoBehaviour {

    private static bool itemThrownToIgnite = false;
    List<GameObject> childList = new List<GameObject>();
    private bool partitionsActivated = false;
    private bool explosionActivated = false;
    public GameObject itemSlot;
    public static bool canIgnite = false;

    public bool isBarrel = true;


    public AudioSource audios;
    public AudioClip BarellExplosion;
    public float audio_volume;
    // Use this for initialization
    void Start () {
        for(int i = 0; i < transform.childCount; i++)
        {
            childList.Add(transform.GetChild(i).gameObject);
            audios = GetComponent<AudioSource>();    
        }

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerStay(Collider other)
    {

        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            if (itemThrownToIgnite)
                StartCoroutine(Ignite()); //it's called when other flammable object throw down the torch but skeleton collides with it

            
            if (itemSlot.transform.childCount > 1)
            {
                GameObject heldItem = itemSlot.gameObject.transform.GetChild(1).gameObject;
                if (heldItem)
                {
                    canIgnite = true;
                    if (Input.GetKey("f") && heldItem.tag == "Torch")
                    {
                        StartCoroutine(Ignite(heldItem));
                        canIgnite = false;
                    }
                }
                else
                {
                    canIgnite = false;
                }
            }    
        }
    }

    void OnTriggerExit(Collider other)
    {
        canIgnite = false;
    }
    IEnumerator Ignite(GameObject heldItem)
    {
        if (heldItem)
        {
            itemThrownToIgnite = true;
        }

        if (isBarrel)
            EnableArsoningParticles();

        yield return new WaitForSeconds(5);

        if (heldItem) Destroy(heldItem);
        itemThrownToIgnite = false;
        GetComponent<MeshRenderer>().enabled = false;

        if ((explosionActivated == false) &&(isBarrel))
        {
            EnableExplosionParticles();
            explosionActivated = true;
        }
        yield return new WaitForSeconds(1);
        canIgnite = false;
        Destroy(gameObject);
    }

    IEnumerator Ignite()
    {
        if ((partitionsActivated == false) && (isBarrel))
            EnableArsoningParticles();

        yield return new WaitForSeconds(5);

        if (!isBarrel)
            this.gameObject.SetActive(false);

        itemThrownToIgnite = false;
        GetComponent<MeshRenderer>().enabled = false;
        if ((explosionActivated == false) && (isBarrel))
        {
            EnableExplosionParticles();
            explosionActivated = true;
        }
        yield return new WaitForSeconds(1);
        canIgnite = false;
        Destroy(gameObject);   
    }

    void EnableArsoningParticles()
    {
        partitionsActivated = true;
        for (int i = 0; i < transform.childCount - 1; i++)
        {
            Debug.Log("Podpalam!");
            childList[i].GetComponent<ParticleSystem>().Play();
        }
    }

    void EnableExplosionParticles()
    {
        childList[childList.Count - 1].GetComponent<ParticleSystem>().Play();
        audios.PlayOneShot(BarellExplosion, audio_volume);
    }
}
