﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

    public  class AwakeMusic : MonoBehaviour
    {
        void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Music");
        if(objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
    }

    }

