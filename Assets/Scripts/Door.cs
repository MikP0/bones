﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour {

    // Use this for initialization
    public GameObject effect;
    private Animator animator;
    private GameObject effectInstance;
    bool doorOpened = false;
	void Start () {
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            if (doorOpened == true)
            {
                effectInstance = Instantiate(effect, transform.position + new Vector3(0, 0, -1), transform.rotation);
            }
            if (KeyManager.isKeyPickedUp == true)
            {
                Debug.Log("Otwieranie drzwi");
                effectInstance = Instantiate(effect, transform.position + new Vector3(0, 0, -1), transform.rotation);
                animator.SetBool("doorOpened", true);
                KeyManager.isKeyPickedUp = false;
                doorOpened = true;
            }
            
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            Destroy(effectInstance, 1);
        }
    }
}
