﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonMove : MonoBehaviour
{
    public float maxSpeed = 8.0f;
    public float moveSpeed = 5.0f;
    public float fallSpeed = 4.0f;
    public float jumpForce = 10.0f;
    public float resetLevel = -10.0f;
    public float gravityScale = 5.0f;
    public static int mode = 0;
    public int specialModeForMBScene = 0;
    public float pitchValue;
    public float volumeValue;

    bool soundDev = false;

    public static bool ladderMode = false;
    public bool ladderClimbAble = false;
    bool stopAllMovementInputs = false;
    bool WaitToClimbLadderTopEndCheck = false;
    float maxSpeedRem = 0.0f;
    float moveSpeedRem = 0.0f;
    float ladderSideRot = 0.0f;

    public int skeletonDirection { get; set; }
    // 1 - x movement
    // 2 - z movement


    AudioSource audioSource;
    CharacterController characterController;
    Animator mAnimator;


    /*
     mode:
     0 - full skeleton
     1 - skull
     3 - right arm
     4 - left arm
     10- skeleton armless
     20- skeleton right arm
     30- skeleton left arm
     */

    float additionalSpeed = 1.0f;
    float zAxisLocation;
    float distToGround;


    Vector3 moveDirection;
    GameObject detectedObject = null;

    GameObject DeadlyBall;


    GameObject myRightArm;


    GameObject[] arms;
    GameObject closest;
    float distance;
    float curDistance;
    Vector3 position;
    Vector3 diff;

    bool danceMode = false;


    // Use this for initialization
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        audioSource = GetComponent<AudioSource>();
        mAnimator = GetComponent<Animator>();
        moveDirection = Vector3.zero;
        zAxisLocation = transform.position.z;
        distToGround = GetComponent<Collider>().bounds.extents.y;
        myRightArm = null;
        maxSpeedRem = maxSpeed;
        moveSpeedRem = moveSpeed;
        skeletonDirection = 1;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (isActiveAndEnabled)
        {
            if ((detectedObject != null) && (hit.gameObject == detectedObject))
            {
                //Debug.Log("STOPED");
                float velocityReduction = Vector3.Dot(moveDirection, hit.normal);
                moveDirection.x = moveDirection.x - (velocityReduction * hit.normal).x;
                additionalSpeed = 1.0f;
            }
        }
    }
    public void WallDetector(GameObject hit)
    {
        detectedObject = hit;
    }


    public void resetSpeed()
    {
        additionalSpeed = 1.0f;
        moveDirection = Vector3.zero;
    }

    bool isGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 1.0f);
    }

    public void rotations(int dir)
    {
        resetSpeed();
        skeletonDirection = dir;
    }

    // Update is called once per frame
    void Update()
    {
        if ((isActiveAndEnabled) && (!stopAllMovementInputs))
        {
            if (ladderMode == false)
            {
                move();
            }
            else if (ladderMode == true)
            {
                ladderClimb();
            }

            if (ladderClimbAble == true)
            {
                if (ladderMode == false)
                {
                    if ((Input.GetKeyDown(KeyCode.F)) && (transform.rotation.y == ladderSideRot))
                    {
                        setLadderMode(true);
                        mAnimator.SetInteger("condition", 0);
                        mAnimator.gameObject.SetActive(false);
                        mAnimator.gameObject.SetActive(true);
                        mAnimator.SetInteger("LadderClimb", 1);
                        maxSpeed = maxSpeed - 4.0f;
                        moveSpeed = moveSpeed - 3.0f;
                    }
                }
                else if (ladderMode == true)
                {
                    if (Input.GetKeyDown(KeyCode.F))
                    {
                        setLadderMode(false);
                        mAnimator.SetInteger("condition", 0);
                        mAnimator.SetInteger("LadderClimb", 0);
                        mAnimator.gameObject.SetActive(false);
                        maxSpeed = maxSpeed + 4.0f;
                        moveSpeed = moveSpeed + 3.0f;
                        mAnimator.gameObject.SetActive(true);
                    }
                }
            }


            if ((specialModeForMBScene != 100) && (!ladderClimbAble))
            {
                if (Input.GetKeyDown(KeyCode.Alpha1))               //drop head
                {
                    //FindObjectOfType<GameMaster>().SkullAndBody(mode);
                    GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().SkullAndBody(mode);
                }

                if (Input.GetKeyDown(KeyCode.Alpha2))
                {
                    if ((mode == 20) || (mode == 0))
                    {
                        //myRightArm = FindObjectOfType<GameMaster>().DropRightArm(mode);  //drop right arm
                        myRightArm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().DropRightArm(mode);  //drop right arm
                    }
                    else if ((mode == 30) || (mode == 10))
                    {
                        if (myRightArm != null)
                        {
                            //FindObjectOfType<GameMaster>().TakeRightArmControl();   // control right arm
                            GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().TakeRightArmControl();   // control right arm
                        }
                    }
                }

                if (mode != 0)
                {
                    if (Input.GetKeyDown(KeyCode.E))                        // collect arm
                    {
                        CollectArm();
                    }
                }
            }








            //dance
            if (Input.GetKeyDown(KeyCode.P))
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                mAnimator.SetTrigger("dance1");
                danceMode = true;
            }

            if (Input.GetKeyDown(KeyCode.O))
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                mAnimator.SetTrigger("dance2");
                danceMode = true;
            }

            if (Input.GetKeyDown(KeyCode.I))
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
                mAnimator.SetTrigger("dance3");
                danceMode = true;
            }
        }

        if (transform.position.y <= resetLevel)                     // DEAD
        {
            GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
        }
    }


    void ladderClimb()
    {
        moveDirection.y = Input.GetAxis("Vertical") * moveSpeed;

        if (moveDirection.y > maxSpeed)
            moveDirection.y = maxSpeed;

        if (moveDirection.y < -maxSpeed)
            moveDirection.y = -maxSpeed;

        if (moveDirection.y != 0)
        {
            mAnimator.SetInteger("LadderClimb", 2);
        }
        else
        {
            mAnimator.SetInteger("LadderClimb", 1);
        }

        characterController.Move(moveDirection * Time.deltaTime);
    }

    public void climbEndTrigger(int number, float angle, float dir)
    {
        if (number == 1)
        {
            mAnimator.SetTrigger("LadderClimbEnd");
        }
        else if (number == 2)
        {
            setLadderMode(true);
            ladderClimbAble = true;
            mAnimator.SetInteger("condition", 0);
            mAnimator.gameObject.SetActive(false);
            mAnimator.gameObject.SetActive(true);
            mAnimator.SetInteger("LadderClimb", 1);
            maxSpeed = maxSpeed - 4.0f;
            moveSpeed = moveSpeed - 3.0f;
            characterController.Move(new Vector3(dir * 0.3f, -0.5f, 0));
            transform.eulerAngles = new Vector3(0, angle, 0);
        }
    }


    public bool getLadderMode()
    {
        return ladderMode;
    }

    public float getMaxSpeedRem()
    {
        return maxSpeedRem;
    }

    void OnAnimatorMove()
    {
        AnimatorClipInfo[] m_CurrentClipInfo = mAnimator.GetCurrentAnimatorClipInfo(0);

        if (m_CurrentClipInfo[0].clip.name.Equals("ClimbLadderTop"))
        {
            StartCoroutine(WaitToClimbLadderTopEnd());
        }
    }

    private IEnumerator WaitToClimbLadderTopEnd()
    {
        mAnimator.ApplyBuiltinRootMotion();
        stopAllMovementInputs = true;

        if (!WaitToClimbLadderTopEndCheck)
        {
            WaitToClimbLadderTopEndCheck = true;
            yield return new WaitForSeconds(0.4f);
            mAnimator.applyRootMotion = false;
            stopAllMovementInputs = false;
            setLadderMode(false);
            mAnimator.SetInteger("condition", 0);
            mAnimator.SetInteger("LadderClimb", 0);
            maxSpeed = maxSpeedRem;
            moveSpeed = moveSpeedRem;
            WaitToClimbLadderTopEndCheck = false;
        }
    }

    void move()
    {
        if (skeletonDirection == 1)
        {
            // fix Z direction
            if (transform.position.z != zAxisLocation)
            {
                moveDirection.z = (zAxisLocation - transform.position.z) * 0.9f;
            }


            moveDirection = new Vector3(Input.GetAxis("Horizontal") * moveSpeed * additionalSpeed, moveDirection.y, moveDirection.z);

            if (moveDirection.x > maxSpeed)
                moveDirection.x = maxSpeed;

            if (moveDirection.x < -maxSpeed)
                moveDirection.x = -maxSpeed;


            if (moveDirection.x == 0.0f && soundDev == true)
            {
                audioSource.pitch = 1.0f;
            }
        }
        else if (skeletonDirection == 2)
        {
            moveDirection = new Vector3(moveDirection.x, moveDirection.y, Input.GetAxis("Horizontal") * moveSpeed * additionalSpeed);

            if (moveDirection.z > maxSpeed)
                moveDirection.z = maxSpeed;

            if (moveDirection.z < -maxSpeed)
                moveDirection.z = -maxSpeed;

            if (moveDirection.z == 0.0f && soundDev == true)
            {
                audioSource.pitch = 1.0f;
            }
        }


        if (characterController.isGrounded)
        {
            moveDirection.y = 0f;

            if (Input.GetButtonDown("Jump"))
            {
                moveDirection.y = jumpForce;
                //mAnimator.SetTrigger("jump");
                mAnimator.SetBool("fall", true);
            }
        }

        moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);


        if (characterController.isGrounded)
        {
            soundDev = true;
            if (characterController.velocity.magnitude > 1f && audioSource.isPlaying == false)
            {
                //audioSource.volume = Random.Range (0.055f, 0.07f);
                audioSource.volume = volumeValue;
                //audioSource.pitch = 1.3f;
                if (audioSource.pitch < pitchValue)
                    audioSource.pitch = audioSource.pitch += 0.05f;
                audioSource.Play();
            }

            additionalSpeed += 0.01f;
        }
        else if (!characterController.isGrounded)
        {
            if (additionalSpeed >= 1)
                additionalSpeed -= 0.005f;
            else
                additionalSpeed = 1.0f;
        }

        characterController.Move(moveDirection * Time.deltaTime);
    }

    public void swapBody()
    {
        GameObject child_full = null;
        GameObject child_arm = null;

        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name.Equals("skeleton_mesh_arm"))
            {
                child_arm = transform.GetChild(i).gameObject;
            }

            if (transform.GetChild(i).name.Equals("skeleton_mesh"))
            {
                child_full = transform.GetChild(i).gameObject;
            }
        }


        if (child_arm.activeSelf == false)
        {
            transform.localScale = new Vector3(-0.2f, 0.2f, 0.2f);
            child_arm.SetActive(true);
            child_full.SetActive(false);
            mode = 30;
        }
        else if (child_full.activeSelf == false)
        {
            transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
            child_full.SetActive(true);
            child_arm.SetActive(false);
            mode = 0;
        }
    }


    public void CollectArm()
    {
        arms = GameObject.FindGameObjectsWithTag("RightArm");
        closest = null;
        distance = Mathf.Infinity;
        position = transform.position;

        foreach (GameObject go in arms)
        {
            diff = go.transform.position - position;
            curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }

        Debug.Log(Vector3.Distance(transform.position, closest.transform.position));
        if ((closest != null) && (Vector3.Distance(transform.position, closest.transform.position) < 1.0f))
        {
            Debug.Log(Vector3.Distance(transform.position, closest.transform.position));
            //FindObjectOfType<GameMaster>().GetArm(mode, closest);
            GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().GetArm(mode, closest);
        }
        else
        {
            closest = null;
            distance = Mathf.Infinity;
            position = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);

            foreach (GameObject go in arms)
            {
                diff = go.transform.position - position;
                curDistance = diff.sqrMagnitude;
                if (curDistance < distance)
                {
                    closest = go;
                    distance = curDistance;
                }
            }

            Debug.Log("HigherGrip\t" + Vector3.Distance(position, closest.transform.position));
            if ((closest != null) && (Vector3.Distance(position, closest.transform.position) < 1.0f))
            {
                Debug.Log(Vector3.Distance(transform.position, closest.transform.position));
                //FindObjectOfType<GameMaster>().GetArm(mode, closest);
                GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().GetArm(mode, closest);
            }
        }
    }

    private void FixedUpdate()
    {
        if ((isActiveAndEnabled) && (!stopAllMovementInputs))
        {
            if (ladderMode == false)
            {
                jumpFallSprint();
            }
        }
    }

    void jumpFallSprint()
    {



        // set fall animation
        bool grounded = Physics.Raycast(transform.position, -Vector3.up, distToGround + 1.5f);
        if (grounded == false)
        {
            mAnimator.SetBool("fall", true);
        }
        else
        {
            mAnimator.SetBool("fall", false);
        }

        if (skeletonDirection == 1)
        {
            // Set sprint animatiom
            if ((moveDirection.x == 0) && (isGrounded()))
            {
                mAnimator.SetInteger("condition", 0);
                additionalSpeed = 1.0f;
            }
            else if (((moveDirection.x >= maxSpeed) || (moveDirection.x <= -maxSpeed)) && (isGrounded()))
            {
                mAnimator.SetInteger("condition", 2);
            }
            else if (((moveDirection.x < 8) || (moveDirection.x > -8)) && (isGrounded()))
            {
                mAnimator.SetInteger("condition", 1);
            }
        }
        else if (skeletonDirection == 2)
        {
            // Set sprint animatiom
            if ((moveDirection.z == 0) && (isGrounded()))
            {
                mAnimator.SetInteger("condition", 0);
                additionalSpeed = 1.0f;
            }
            else if (((moveDirection.z >= maxSpeed) || (moveDirection.z <= -maxSpeed)) && (isGrounded()))
            {
                mAnimator.SetInteger("condition", 2);
            }
            else if (((moveDirection.z < 8) || (moveDirection.z > -8)) && (isGrounded()))
            {
                mAnimator.SetInteger("condition", 1);
            }
        }
    }

    private void LateUpdate()
    {
        if ((isActiveAndEnabled) && (!stopAllMovementInputs))
        {
            if (ladderMode == false)
            {
                changeRotateSide();
            }
        }
    }

    void changeRotateSide()
    {
        if (skeletonDirection == 1)
        {

            if (Input.GetAxis("Horizontal") < 0)
            {
                transform.eulerAngles = new Vector3(0, -90, 0);
            }
            else if (Input.GetAxis("Horizontal") > 0)
            {
                transform.eulerAngles = new Vector3(0, 90, 0);
            }
        }
        else if (skeletonDirection == 2)
        {
            if (Input.GetAxis("Horizontal") < 0)
            {
                transform.eulerAngles = new Vector3(0, 180, 0);
            }
            else if (Input.GetAxis("Horizontal") > 0)
            {
                transform.eulerAngles = new Vector3(0, 0, 0);
            }
        }
    }

    private void OnEnable()
    {
        moveDirection = Vector3.zero;
    }

    public void diedAnimation()
    {
        mAnimator.SetTrigger("FallDied");
    }

    public void setLadderMode(bool mode)
    {
        ladderMode = mode;
        additionalSpeed = 1.0f;
    }

    public void setLadderClimbAble(bool able, float rotateSide)
    {
        ladderSideRot = rotateSide;
        ladderClimbAble = able;
    }
}
