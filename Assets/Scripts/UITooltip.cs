﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UITooltip : MonoBehaviour {

    public TextMeshProUGUI textPro;
    public string content;
    public float textFadeTime = 1.0f;
    public enum TooltipTarget { Everyone, FullSkeleton, Skull, Arm };
    public TooltipTarget tooltipTarget = TooltipTarget.Everyone;


    private void Start()
    {
        textPro.color = new Color(1, 1, 1, 0);
        ((RawImage)textPro.GetComponentInChildren(typeof(RawImage))).color = new Color(1, 1, 1, 0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerSkeletonFull" && (tooltipTarget == TooltipTarget.Everyone || tooltipTarget == TooltipTarget.FullSkeleton))
        {
            showText();
        }

        else if(other.tag == "PlayerSkull" && (tooltipTarget == TooltipTarget.Everyone || tooltipTarget == TooltipTarget.Skull))
        {
            showText();
        }

        else if (other.tag == "RightArm" && (tooltipTarget == TooltipTarget.Everyone || tooltipTarget == TooltipTarget.Arm))
        {
            showText();
        }

    }

    private void showText()
    {
        textPro.text = content;
        StartCoroutine(FadeTo(1.0f, textFadeTime));
    }


    private void OnTriggerExit(Collider other)
    {
       // if (other.tag == "PlayerSkeletonFull")
            StartCoroutine(FadeTo(0.0f, textFadeTime));
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        float alpha = textPro.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
            textPro.color = newColor;
            ((RawImage)textPro.GetComponentInChildren(typeof(RawImage))).color = newColor;
            yield return null;
        }
    }
}
