﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmMove : MonoBehaviour
{
    public float speed = 0.1f;
    public float maxSpeed = 0.05f;
    public int resetLevel = -10;

    float xSpeed;
    float ySpeed;
    float additonalSpeedX;
    float additonalSpeedY;
    float velocityX;
    float velocityY;

    Rigidbody mRigidbody = null;
    Vector3 movement;
    Animator mAnimator;

    private bool climeAble = false;
    private bool climMode = false;
    private int walls = 0;

    public void setWalls(int i)
    {
        if (i <= 10)
        {
            walls += i;
        }
        else if (i == 100)
        {
            walls = 0;
        }
    }


    public void startClimb()
    {
        climeAble = true;
    }

    public void stopClimb()
    {
        if (mRigidbody != null)
        {
            if (climMode == true)
            {
                if (walls == 0)
                {
                    climeAble = false;
                    Quaternion quaternion = Quaternion.Euler(new Vector3(0, 0, 0));
                    transform.rotation = quaternion;
                    climMode = false;
                    ySpeed = 0.0f;
                    additonalSpeedY = 0.0f;
                    mRigidbody.drag = 0.0f;
                    mRigidbody.useGravity = true;
                }
            }
            else if (walls == 0)
            {
                climeAble = false;
                Quaternion quaternion = Quaternion.Euler(new Vector3(0, 0, 0));
                transform.rotation = quaternion;
                climMode = false;
                ySpeed = 0.0f;
                additonalSpeedY = 0.0f;
                mRigidbody.drag = 0.0f;
                mRigidbody.useGravity = true;
            }
        }
    }

    // Use this for initialization
    public void Start()
    {
        mRigidbody = GetComponent<Rigidbody>();
        mAnimator = GetComponent<Animator>();
        xSpeed = 0.0f;
        ySpeed = 0.0f;
        additonalSpeedX = 0.0f;
        additonalSpeedY = 0.0f;
        velocityX = 0.0f;
        velocityY = 0.0f;
        movement = Vector3.zero;
    }

    private void OnDisable()
    {
        if (climMode == false)
        {
            mRigidbody.useGravity = true;          
        }

        additonalSpeedX = 0.0f;
        additonalSpeedY = 0.0f;
        velocityX = 0.0f;
        velocityY = 0.0f;
        movement = Vector3.zero;
        mAnimator.SetInteger("Move", 0);      
    }

    // Update is called once per frame
    void Update()
    {
        if (isActiveAndEnabled)
        {
            if ((Input.GetKeyDown(KeyCode.Alpha1)) || (Input.GetKeyDown(KeyCode.E)) || (Input.GetKeyDown(KeyCode.Alpha2)))
            {
                mAnimator.SetInteger("Move", 0);
                //FindObjectOfType<GameMaster>().BackSkeletonControl();
                GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().BackSkeletonControl();
            }

            if ((Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKey(KeyCode.A)))
            {
                xSpeed = -1;
                additonalSpeedX += speed;
                mAnimator.SetInteger("Move", 1);
                if (climMode == false)
                    transform.eulerAngles = new Vector3(0, 180, 0);
            }
            if ((Input.GetKey(KeyCode.RightArrow)) || (Input.GetKey(KeyCode.D)))
            {
                xSpeed = 1;
                additonalSpeedX += speed;
                mAnimator.SetInteger("Move", 1);
                if (climMode == false)
                    transform.eulerAngles = new Vector3(0, 0, 0);
            }

            if ((Input.GetKeyUp(KeyCode.RightArrow)) || (Input.GetKeyUp(KeyCode.D)) || (Input.GetKeyUp(KeyCode.LeftArrow)) || (Input.GetKeyUp(KeyCode.A)))
            {
                xSpeed = 0;
                additonalSpeedX = 0.0f;
            }

            if (climMode == false)
            {
                if ((Input.GetKeyDown(KeyCode.DownArrow)) || (Input.GetKeyDown(KeyCode.S)))
                {
                    xSpeed = 0;
                    mRigidbody.velocity = Vector3.zero;
                    additonalSpeedX = 0.0f;
                }
            }

            if (mRigidbody.position.y <= resetLevel)                     // DEAD
            {
                //FindObjectOfType<GameManager>().EndGame();
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().EndGame();
            }

            if (climeAble == true)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    if (climMode == false)
                    {
                        climMode = true;
                        mRigidbody.useGravity = false;
                        mRigidbody.drag = 50.0f;
                        Quaternion quaternion = Quaternion.Euler(new Vector3(0, -90, 90));
                        transform.rotation = quaternion;
                        additonalSpeedX = 0.0f;
                        additonalSpeedY = 0.0f;
                    }
                    else if (climMode == true)
                    {
                        Quaternion quaternion = Quaternion.Euler(new Vector3(0, 0, 0));
                        transform.rotation = quaternion;
                        climMode = false;
                        mRigidbody.drag = 0.0f;
                        mRigidbody.useGravity = true;
                        additonalSpeedX = 0.0f;
                        additonalSpeedY = 0.0f;
                    }
                }

                if (climMode == true)
                {
                    if ((Input.GetKey(KeyCode.UpArrow)) || (Input.GetKey(KeyCode.W)))
                    {
                        ySpeed = 1;
                        additonalSpeedY += speed;
                        mAnimator.SetInteger("Move", 1);
                    }
                    if ((Input.GetKey(KeyCode.DownArrow)) || (Input.GetKey(KeyCode.S)))
                    {
                        ySpeed = -1;
                        additonalSpeedY += speed;
                        mAnimator.SetInteger("Move", 1);
                    }
                    if ((Input.GetKeyUp(KeyCode.UpArrow)) || (Input.GetKeyUp(KeyCode.W)) || (Input.GetKeyUp(KeyCode.DownArrow)) || (Input.GetKeyUp(KeyCode.S)))
                    {
                        xSpeed = 0;
                        ySpeed = 0;
                        additonalSpeedX = 0.0f;
                        additonalSpeedY = 0.0f;
                    }
                }
            }
        }
    }

    private void FixedUpdate()
    {
        if (isActiveAndEnabled)
        {
            if (climMode != true)
            {
                // movement X
                velocityX = xSpeed * Time.deltaTime * additonalSpeedX;

                if (velocityX > maxSpeed)
                    velocityX = maxSpeed;
                else if (velocityX < -maxSpeed)
                    velocityX = -maxSpeed;

                movement = new Vector3(velocityX, 0, 0);
                mRigidbody.MovePosition(transform.position + movement);
            }
            else if (climMode == true)
            {
                // movement Y and X
                velocityY = ySpeed * Time.deltaTime * additonalSpeedY;
                velocityX = xSpeed * Time.deltaTime * additonalSpeedX;


                if (velocityY > maxSpeed)
                    velocityY = maxSpeed;
                else if (velocityY < -maxSpeed)
                    velocityY = -maxSpeed;

                if (velocityX > maxSpeed)
                    velocityX = maxSpeed;
                else if (velocityX < -maxSpeed)
                    velocityX = -maxSpeed;

                movement = new Vector3(velocityX, velocityY, 0);
                mRigidbody.MovePosition(transform.position + movement);
            }
        }
    }

    private void LateUpdate()
    {
        if (isActiveAndEnabled)
        {
            if (velocityX == 0)
            {
                mAnimator.SetInteger("Move", 0);
            }
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "RightArm")
        {
            Debug.Log("Avoid arm");
            Physics.IgnoreCollision(collision.collider, GetComponent<Collider>());
        }
    }
}