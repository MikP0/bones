﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{


    public GameObject door;

    public LayerMask m_LayerMask;
    Animator anim;

    public enum ButtonTarget { Everyone, FullSkeleton, Skull, Arm, Body };
    public ButtonTarget buttonTarget = ButtonTarget.Everyone;

    List<GameObject> colliderList = new List<GameObject>();


    void Start()
    {
        anim = door.GetComponent<Animator>();
    }


    void FixedUpdate()
    {
        colliderList.Clear();
    }

    void Update()
    {
        var numberOfColliders = colliderList.Count;
        //colliderList.Clear();
    }
    void OnTriggerEnter(Collider other)
    {
        if (!colliderList.Contains(other.gameObject))
        {
            if (other.tag == "PlayerSkeletonFull" && (buttonTarget == ButtonTarget.Everyone || buttonTarget == ButtonTarget.FullSkeleton))
            {
                colliderList.Add(other.gameObject);
                anim.SetBool("isOpen", true);
            }

            else if (other.tag == "PlayerSkull" && (buttonTarget == ButtonTarget.Everyone || buttonTarget == ButtonTarget.Skull))
            {
                colliderList.Add(other.gameObject);
                anim.SetBool("isOpen", true);
            }

            else if (other.tag == "RightArm" && (buttonTarget == ButtonTarget.Everyone || buttonTarget == ButtonTarget.Arm))
            {
                colliderList.Add(other.gameObject);
                anim.SetBool("isOpen", true);
            }
        }
        //Debug.Log(colliderList.Count);
    }

    private void OnTriggerStay(Collider other)
    {
        OnTriggerEnter(other);
    }

    void OnTriggerExit(Collider other)
    {
        colliderList.Remove(other.gameObject);
        if (colliderList.Count == 0)
            anim.SetBool("isOpen", false);
    }

}
