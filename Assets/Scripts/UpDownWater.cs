﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpDownWater : MonoBehaviour {

    public GameObject water;

    public LayerMask m_LayerMask;
  //  Animator anim;

    List<GameObject> colliderList = new List<GameObject>();
    private killer_water killerWater;

    void Start()
    {
        //anim = water.GetComponent<Animator>();
        //killerWater = FindObjectOfType<killer_water>();
        killerWater = water.GetComponent<killer_water>();
    }


    void FixedUpdate()
    {
        colliderList.Clear();
    }

    void Update()
    {
        var numberOfColliders = colliderList.Count;
        //colliderList.Clear();
    }
    void OnTriggerEnter(Collider other)
    {

        if (!colliderList.Contains(other.gameObject))
        {
            colliderList.Add(other.gameObject);
            //anim.SetBool("isOpen", true);
            //FindObjectOfType<killer_water>().WaterUp();
            killerWater.WaterUp();
        }
        //Debug.Log(colliderList.Count);
    }

    private void OnTriggerStay(Collider other)
    {
        OnTriggerEnter(other);
    }

    void OnTriggerExit(Collider other)
    {
        colliderList.Remove(other.gameObject);
        if (colliderList.Count == 0)
            // anim.SetBool("isOpen", false);
            //FindObjectOfType<killer_water>().WaterDown();
            killerWater.WaterDown();
    }
}
