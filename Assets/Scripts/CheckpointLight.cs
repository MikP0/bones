﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointLight : MonoBehaviour
{
    public GameObject light1 = null;
    public GameObject light2 = null;
    public GameObject light3 = null;

    public bool lightOn = false;

    public void LightOn()
    {
        if ((light1 != null) && (light2 != null) && (light3 != null) && (lightOn == false))
        {
            light1.SetActive(true);
            light2.SetActive(true);
            light3.SetActive(true);
            lightOn = true;
        }
    }

    public void LightOff()
    {
        if ((light1 != null) && (light2 != null) && (light3 != null))
        {
            light1.SetActive(false);
            light2.SetActive(false);
            light3.SetActive(false);
            lightOn = false;
        }
    }
}
