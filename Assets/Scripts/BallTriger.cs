﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTriger : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PlayerSkeletonFull")
        {
            GameObject.FindGameObjectWithTag("KillerBall").GetComponent<Rigidbody>().isKinematic = false;
            GameObject.FindGameObjectWithTag("KillerBall").GetComponent<Rigidbody>().AddForce((Vector3.down * 180.0f), ForceMode.Acceleration);
            //FindObjectOfType<KillPlayer>().GetComponent<Rigidbody>().isKinematic = false;
        }
        else if (other.gameObject.tag == "PlayerSkull")
        {
            //FindObjectOfType<KillPlayer>().GetComponent<Rigidbody>().isKinematic = false;
            GameObject.FindGameObjectWithTag("KillerBall").GetComponent<Rigidbody>().isKinematic = false;
        }
        else if (other.gameObject.tag == "RightArm")
        {
            //FindObjectOfType<KillPlayer>().GetComponent<Rigidbody>().isKinematic = false;
            GameObject.FindGameObjectWithTag("KillerBall").GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
