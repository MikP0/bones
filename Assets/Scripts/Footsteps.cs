﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour {

	CharacterController cc;
	AudioSource audioSource;

	void Start () {

		cc = GetComponent<CharacterController> ();
		audioSource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (cc.isGrounded == true && cc.velocity.magnitude > 2f) {
			audioSource.Play ();
		}
	}
}
