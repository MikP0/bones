﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class LoadSoundVolume : MonoBehaviour {

    public AudioSource music;
    public Scrollbar slider;
    private float musicVol = 1f;

    void Start()
    {
        musicVol = loadVolume();
        slider.value = loadVolume();
        music = GetComponent<AudioSource>();
    }

    void Update()
    {
        music.volume = musicVol;
    }
    public void SetVolume(float vol)
    {
        musicVol = vol;
    }

    float loadVolume()
    {
        float readMeText;
        using (StreamReader readtext = new StreamReader("volume.txt"))
        {
             readMeText = float.Parse(readtext.ReadLine());
        }
        return readMeText;
    }


}
