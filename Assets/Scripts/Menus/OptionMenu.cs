﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OptionMenu : MonoBehaviour
{

    public GameObject pauseMenu;
    public GameObject mainMenu;


    public void Resume()
    {
        pauseMenu.SetActive(false);
        mainMenu.SetActive(true);
        Time.timeScale = 0f;

    }

    void Pause()
    {
        mainMenu.SetActive(false);
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;

    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {

                Resume();
           
        }
    }

}
