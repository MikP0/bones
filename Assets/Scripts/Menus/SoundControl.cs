﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SoundControl : MonoBehaviour
{

    public AudioSource music;
    private  float musicVol = 1f;

    void Start()
    {
        music = GetComponent<AudioSource>();
    }

    void Update()
    {
        music.volume = musicVol;
    }
    public void SetVolume(float vol)
    {
        musicVol = vol;
        saveVolume(musicVol);
    }

     void saveVolume(float volume)
    {
        using (StreamWriter writetext = new StreamWriter("volume.txt"))
        {
            writetext.WriteLine(volume.ToString());
        }
    }



}