﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {
    public GameObject lvl1;
    public GameObject lvl2;
    public GameObject lvl3;
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex +1);
    }

    public void lvlMenu()
    {
        lvl1.SetActive(true);
        lvl2.SetActive(true);
        lvl3.SetActive(true);

    }

    public void lvl1Run()
    {
        SceneManager.LoadScene(1);
    }

    public void lvl2Run()
    {
        SceneManager.LoadScene(2);
    }

    public void lvl3Run()
    {
        SceneManager.LoadScene(3);
    }

    public void QuitGame()
    {

        Application.Quit();
    }

}
