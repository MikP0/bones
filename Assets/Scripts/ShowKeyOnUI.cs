﻿using UnityEngine;
using TMPro;

public class ShowKeyOnUI : MonoBehaviour {

    public TextMeshProUGUI textKeyF;
    public TextMeshProUGUI textKeyE;
    public TextMeshProUGUI textKey1;
    public TextMeshProUGUI textKey2;

    private GameObject canvas;
    private GameObject iconF;
    private GameObject iconE;
    private GameObject icon1;
    private GameObject icon2;

    private CameraScrpit cameraScript;
    private GameObject cameraTarget;
    // Use this for initialization
    void Start () {
        canvas = GameObject.Find("Canvas");
        iconF = canvas.transform.Find("IconF").gameObject;
        iconF.SetActive(false);
        iconE = canvas.transform.Find("IconE").gameObject;
        iconE.SetActive(false);
        icon1 = canvas.transform.Find("Icon1").gameObject;
        icon2 = canvas.transform.Find("Icon2").gameObject;
        cameraScript = GameObject.Find("Main Camera").gameObject.GetComponent<CameraScrpit>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        CheckifChangingStateIsNecessary(iconF, false);
        CheckifChangingStateIsNecessary(iconE, false);
        CheckifChangingStateIsNecessary(icon1, true);
        CheckifChangingStateIsNecessary(icon2, true);
        int skeletonMode = SkeletonMove.mode;
        cameraTarget = cameraScript.target;
        if (cameraTarget.tag == "PlayerSkeletonFull") 
        {
            PickUpItemsUI();
            ItemToIgniteUI();
            ElevatorUI();
            LadderUI();
            TimerLeverUI();
        }

        if (cameraTarget.tag == "RightArm")
        {
            ElevatorUI();
        }

        MergingBonesInfo();
        SkeletonModesInfo(ref skeletonMode);
    }

    void PickUpItemsUI()
    {
        if (ObjectToHolding.itemInRange && ObjectToHolding.objectHeld == null)
        {
            CheckifChangingStateIsNecessary(iconF, true);
            ChangeTextIfPossible(textKeyF, "Pick a torch");
        }
        else
        {
            ChangeTextIfPossible(textKeyF, "Drop a torch");
        }

        if(ObjectToHolding.lowerUsagePriority && ObjectToHolding.objectHeld)
        {
            CheckifChangingStateIsNecessary(iconF, false);
        }
        else
        {
            CheckifChangingStateIsNecessary(iconF, true);
        }

        if (ObjectToHolding.itemInRange == false)
        {
            CheckifChangingStateIsNecessary(iconF, false);
        }
    }
    void ItemToIgniteUI()
    {
        if (Arsoning.canIgnite)
        {
            CheckifChangingStateIsNecessary(iconF, true);
            ChangeTextIfPossible(textKeyF, "Ignite");
        }
    }
    void ElevatorUI()
    {
        if (LeverManager.PlayerOnElevator)
        {
            CheckifChangingStateIsNecessary(iconF, true);
            ChangeTextIfPossible(textKeyF, "Change floor");
        }
        if (LeverManager.PlayerOnElevatorCaller)
        {
            CheckifChangingStateIsNecessary(iconF, true);
            ChangeTextIfPossible(textKeyF, "Call elevator");
        }
        
    }
    void LadderUI()
    {
        if (LadderCollision.playerInRange)
        {
            CheckifChangingStateIsNecessary(iconF, true);
            ChangeTextIfPossible(textKeyF, "Climb");
        }
        if (SkeletonMove.ladderMode)
        {
            CheckifChangingStateIsNecessary(iconF, true);
            ChangeTextIfPossible(textKeyF, "Get off the ladder");
        }
    }
    void TimerLeverUI()
    {
        if (TimerLever.playerInRange)
        {
            CheckifChangingStateIsNecessary(iconF, true);
            ChangeTextIfPossible(textKeyF, "Push the lever");
        }
    }
    void MergingBonesInfo()
    {
        if (UIContentManager.pickUp || SkullMove.pickUp)
        {
            CheckifChangingStateIsNecessary(iconE, true);
            ChangeTextIfPossible(textKeyE, "Merge bones");
        }
        if (!UIContentManager.pickUp && SkullMove.merged)
        {
            CheckifChangingStateIsNecessary(iconE, false);
        }
    }
    void SkeletonModesInfo(ref int skeletonMode)
    {
        if (cameraTarget.tag == "PlayerSkeletonFull" && skeletonMode == 0)
        {
            ChangeTextIfPossible(textKey1, "Drop skull");
            ChangeTextIfPossible(textKey2, "Drop arm");
        }
        if (cameraTarget.tag == "PlayerSkeletonFull" && skeletonMode == 30)
        {
            ChangeTextIfPossible(textKey1, "Drop skull");
            ChangeTextIfPossible(textKey2, "Switch to arm");
        }
        if (cameraTarget.tag == "RightArm" && skeletonMode == 30)
        {
            CheckifChangingStateIsNecessary(icon1, false);
            ChangeTextIfPossible(textKey2, "Switch to skeleton");
        }
        if (cameraTarget.tag == "PlayerSkull")
        {
            CheckifChangingStateIsNecessary(icon1, false);
            CheckifChangingStateIsNecessary(icon2, false);
            CheckifChangingStateIsNecessary(iconF, false);
        }
    }

    void CheckifChangingStateIsNecessary(GameObject UI_Element, bool desiredState)
    {
        if (UI_Element.activeSelf != desiredState)
        {
            UI_Element.SetActive(desiredState);
        }
    }

    void ChangeTextIfPossible(TextMeshProUGUI textField, string description)
    {
        //if (textField.transform.parent.gameObject.activeSelf == true)
        //{
        if (textField.text != description)
        {
            textField.text = description;
        }
        //}
    }
}
