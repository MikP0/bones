﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonWallDetector : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (!collision.collider.tag.Equals("PlayerSkeletonFull"))
        {
            FindObjectOfType<SkeletonMove>().WallDetector(collision.gameObject);
        }
    }
    
    private void OnCollisionExit(Collision collision)
    {
        if (!collision.collider.tag.Equals("PlayerSkeletonFull"))
        {
            FindObjectOfType<SkeletonMove>().WallDetector(null);
        }
    }
}
