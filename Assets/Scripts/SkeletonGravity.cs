﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonGravity : MonoBehaviour
{
    public float maxSpeed = 8.0f;
    public float moveSpeed = 5.0f;
    public float fallSpeed = 4.0f;
    public float jumpForce = 5.0f;
    public float resetLevel = -10.0f;
    private float gravityScale = 0.7f;
    // public int mode = 0;

    Vector3 moveDirection;
    float distToGround;

    CharacterController characterController;
    Animator mAnimator;

    bool isGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
    }

    // Use this for initialization
    void Start ()
    {
        characterController = GetComponent<CharacterController>();
        moveDirection = Vector3.zero;
        distToGround = GetComponent<Collider>().bounds.extents.y;
        mAnimator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        moveDirection.y += (Physics.gravity.y * gravityScale * Time.deltaTime);

        if (moveDirection.y < -fallSpeed)
            moveDirection.y = -fallSpeed;

        characterController.Move(moveDirection * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        // set fall animation
        bool grounded = Physics.Raycast(transform.position, -Vector3.up, distToGround - 0.1f);
        if (grounded == false)
        {
            mAnimator.SetBool("fall", true);
        }
        else
        {
            mAnimator.SetBool("fall", false);
        }
    }
}
