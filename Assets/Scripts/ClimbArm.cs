﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbArm : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("RightArm"))
        {
            other.GetComponent<ArmMove>().setWalls(1);
            other.GetComponent<ArmMove>().startClimb();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag.Equals("RightArm"))
        {
            other.GetComponent<ArmMove>().setWalls(-1);
            other.GetComponent<ArmMove>().stopClimb();
        }
    }
}
