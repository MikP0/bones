﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIContent_Skull : MonoBehaviour {
    public GameObject canvas;

    GameObject[] parts;
    GameObject closest;
    float distance;
    float curDistance;
    Vector3 position;
    Vector3 diff;

    // Update is called once per frame
    void Update () {
        parts = GameObject.FindGameObjectsWithTag("PartBody");

        closest = null;
        distance = Mathf.Infinity;
        position = transform.position;


        foreach (GameObject go in parts)
        {
            diff = go.transform.position - position;
            curDistance = diff.sqrMagnitude;
            if (curDistance < distance)
            {
                closest = go;
                distance = curDistance;
            }
        }
        if ((closest != null) && (Vector3.Distance(transform.position, closest.transform.position) < 1.0f))
        {
            canvas.transform.GetChild(0).gameObject.SetActive(true);

        }
        else
        {
            canvas.transform.GetChild(0).gameObject.SetActive(false);
        }

  
    }

    void OnDisable()
    {
        canvas.transform.GetChild(0).gameObject.SetActive(false);
    }
}

