﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnstablePlatform : MonoBehaviour {

	public GameObject Player;
	public GameObject SecondBox;
	public Collider collider_left;
	public Collider collider_right;
	private float backTime;
	private float timeElapsed;
	private float rotation;

	private float turningTime = 0f;
	Quaternion from;
	Quaternion to;

	void Start()
	{
		from = Quaternion.Euler(new Vector3(0f,0f,0f));
		to = Quaternion.Euler(new Vector3(0f,180f,0f));
	}

	void OnTriggerEnter(Collider other)
	{
		//timeElapsed = 0;
	}

	void OnTriggerStay(Collider other)
	{
		//if (other.gameObject.Equals(Player))
		{
			timeElapsed += Time.deltaTime;
			backTime = Time.deltaTime;
			transform.Rotate(Vector3.forward * 40.0f * backTime);
			SecondBox.transform.Rotate (Vector3.forward * 40.0f * backTime);
		}
	}

	void Update()
	{
		transform.Rotate(Vector3.forward * 40.0f * backTime);
		SecondBox.transform.Rotate (Vector3.forward * 40.0f * backTime);
	}
}
