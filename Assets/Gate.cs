﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {

	Animator animator;
	public GameObject playerObject;
    public AudioSource audios;
    public AudioClip gateSound;
    public float volume;

	void Start () 
	{
		animator = GetComponent<Animator> ();
        audios = GetComponent<AudioSource>();
	}
	
	void Update () 
	{
	}

	void OnTriggerEnter(Collider player)
	{
		if (player.gameObject.Equals (playerObject)) 
		{
            Debug.Log("TAK");
			animator.SetTrigger ("open");
		}
	}

	void gateReady()
	{
        animator.speed = 0;
	}

    void gateSoundPlay()
    {
        audios.PlayOneShot(gateSound, volume);
    }
}
