﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushableBox : MonoBehaviour {

    public Rigidbody rigidbody;
    public AudioSource audios;
    public AudioClip pushingSound;
    public float volume;


	void Start()
	{
		rigidbody = GetComponent<Rigidbody>();
	}

    void Update()
    {
        if (rigidbody.velocity.magnitude >= 0.2)
        {
            if (!audios.isPlaying)
            {
                audios.PlayOneShot(pushingSound, volume);
            }
        }
    }
}